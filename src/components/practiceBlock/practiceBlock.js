import React from "react"

import practiceBlockStyles from "./practiceBlock.module.scss"

class praticeBlock extends React.Component {
    render() {
        var darkBg = this.props.darkBg;
        return (
            <div className={[practiceBlockStyles.practiceBlock, (darkBg ? '' : practiceBlockStyles.darkBg)].join(' ')}>
                <div className={practiceBlockStyles.practiceImgBlock}>
                    <div className={practiceBlockStyles.practiceImgItem}></div>
                </div>
                <div className={practiceBlockStyles.practiceTextBlock}>
                    <p className={["textBlack", practiceBlockStyles.practiceTextItem].join(' ')}>{this.props.titleText}</p>
                    <p className={[practiceBlockStyles.practiceTextItem].join(' ')}>{this.props.practiceText}</p>
                    <div className={[practiceBlockStyles.chromeBlock].join(' ')}><div className={practiceBlockStyles.chromeIcon}></div>Chromecast</div>
                </div>
            </div>
        )
    }
}

export default praticeBlock