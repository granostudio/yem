import React from "react"
import filterMenuStyles from "./filterMenu.module.scss"
import { Link } from "gatsby"
import SelectField from "../selectField/selectField"

import $ from "jquery"

class filterMenu extends React.Component {
    componentDidMount() {
        $(document).ready(function () {
            $(window).on("resize", function (e) {
                checkScreenSize();
            });
        
            var filterIconBlock = $('#filterIconBlock');
            var filterContent = $('#filterContent');
            checkScreenSize();
        
            function checkScreenSize(){
                var newWindowWidth = $(window).width();
                if (newWindowWidth <= 800) {
                    filterIconBlock.click( function(){
                        filterContent.toggleClass(filterMenuStyles.filterContentActive);
                    })
                }
            }
        });
    }
    render(){
        return(
            <div className={filterMenuStyles.filter}>
                <p className={filterMenuStyles.topText}>O que você quer assistir hoje?</p>
                <div className={filterMenuStyles.filterBlock}>
                    <div className={filterMenuStyles.filterIcon} id={'filterIconBlock'}>
                        <div className={["iconYem params fontLightGrey", filterMenuStyles.icon ].join(' ')}></div>
                        <p className={[filterMenuStyles.filterIconText, "fontLightGrey"].join(' ')}>Filtrar as aulas</p>
                    </div>
                    <div className={[filterMenuStyles.filterContent].join(' ')} id={"filterContent"}>
                        <div className={[filterMenuStyles.filterButton, filterMenuStyles.active, "roundBorderSmall"].join(' ')}>Tudo</div>
                        <a href="/" className={[filterMenuStyles.filterButton, "roundBorderSmall"].join(' ')}>ASSISTIDAS</a>
                        <Link to="/" className={[filterMenuStyles.filterButton, "roundBorderSmall"].join(' ')} >FAVORITAS</Link>
                        <SelectField
                            placeholder={'PROFESSOR'}
                            options={[
                                { value: 'chocolate', label: 'Chocolate' },
                                { value: 'strawberry', label: 'Strawberry' },
                                { value: 'vanilla', label: 'Vanilla' },
                                { value: 'chocolate', label: 'Chocolate' },
                                { value: 'strawberry', label: 'Strawberry' },
                                { value: 'chocolate', label: 'Chocolate' },
                                { value: 'strawberry', label: 'Strawberry' },
                            ]}
                        />
                        <SelectField 
                            placeholder={'DURAÇÃO'}
                            options={[
                                { value: 'chocolate', label: 'Chocolate' },
                                { value: 'strawberry', label: 'Strawberry' },
                                { value: 'vanilla', label: 'Vanilla' }
                            ]}
                        />
                        <SelectField 
                            placeholder={'DIFICULDADE'}
                            options={[
                                { value: 'chocolate', label: 'Chocolate' },
                                { value: 'strawberry', label: 'Strawberry' },
                                { value: 'vanilla', label: 'Vanilla' }
                            ]}
                        />
                        <SelectField 
                            placeholder={'ORDEM'}
                            options={[
                                { value: 'chocolate', label: 'Chocolate' },
                                { value: 'strawberry', label: 'Strawberry' },
                                { value: 'vanilla', label: 'Vanilla' }
                            ]}
                        />
                        
                    </div>
                </div>
            </div>
        )
    }
} 
export default filterMenu