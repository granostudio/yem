import React from "react"
import advantagesListBlockStyles from "./advantagesListBlock.module.scss"

class advantagesListBlock extends React.Component {
    render() {
        return(
            <div className={["displayFlex", advantagesListBlockStyles.advantageList].join(' ')}>
                <div className={["smBlock", advantagesListBlockStyles.advantageItem].join(' ')}>
                    <div className={[advantagesListBlockStyles.itemBgBlock].join(' ')} style={{backgroundImage: ['url(', this.props.firstBg, ')'].join(' '), backgroundPosition: 'bottom'}}>

                    </div>
                    <div className={["bgLightGrey roundBorderDefault", advantagesListBlockStyles.itemTextBlock].join(' ')}>
                        <h4 className={advantagesListBlockStyles.advantageTitle}>Pratique online</h4>
                        <p>Sem trânsito</p>
                        <p>Sem stress</p>
                        <p>Preço baixo</p>
                    </div>
                </div>

                <div className={["smBlock", advantagesListBlockStyles.advantageItem].join(' ')}>
                    <div className={[advantagesListBlockStyles.itemBgBlock].join(' ')} style={{backgroundImage: ['url(', this.props.secondBg, ')'].join(' ')}}>

                    </div>
                    <div className={["bgLightGrey roundBorderDefault", advantagesListBlockStyles.itemTextBlock].join(' ')}>
                        <h4 className={advantagesListBlockStyles.advantageTitle}>Mude sua vida</h4>
                        <p>Bem estar</p>
                        <p>Vitalidade</p>
                        <p>Saúde</p>
                    </div>
                </div>

                <div className={["smBlock", advantagesListBlockStyles.advantageItem].join(' ')}>
                    <div className={[advantagesListBlockStyles.itemBgBlock].join(' ')} style={{backgroundImage: ['url(', this.props.thirdBg, ')'].join(' ')}}>

                    </div>
                    <div className={["bgLightGrey roundBorderDefault", advantagesListBlockStyles.itemTextBlock].join(' ')}>
                        <h4 className={advantagesListBlockStyles.advantageTitle}>Descubra-se</h4>
                        <p>Autoconhecimento</p>
                        <p>Evolução inteirior</p>
                        <p>Felicidade plena</p>
                    </div>
                </div>
                
            </div>
        )
    }
}
export default advantagesListBlock 
