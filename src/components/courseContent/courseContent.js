import React from "react"
import courseContentStyles from "./courseContent.module.scss"

class courseContent extends React.Component {
    render() {
        return(
            <div className={courseContentStyles.course}>
                <h3 className={["alignCenter fontLightGrey", courseContentStyles.courseTitle].join(' ')}>Conteúdo do curso</h3>
                <div className={[courseContentStyles.courseList].join(' ')}>
                    <div className={[courseContentStyles.listFirstItens, "displayFlex flexColumn"].join(' ')}>
                        <div className={[courseContentStyles.courseItem, "bgLightGrey roundBorderDefault"].join(' ')}>
                            <div className={[courseContentStyles.courseItemImgBlock, "roundBorderFull"].join(' ')}></div>
                            <div className={courseContentStyles.courseItemContent}>
                                <h4>Modulo 1</h4>
                                <ul>
                                    <li>item1</li>
                                    <li>item1</li>
                                    <li>item1</li>
                                    <li>item1</li>
                                </ul>
                            </div>
                        </div>
                        <div className={[courseContentStyles.courseItem, "bgLightGrey roundBorderDefault"].join(' ')}>
                            <div className={[courseContentStyles.courseItemImgBlock, "roundBorderFull"].join(' ')}></div>
                            <div className={courseContentStyles.courseItemContent}>
                                <h4>Modulo 1</h4>
                                <ul>
                                    <li>item1</li>
                                    <li>item1</li>
                                    <li>item1</li>
                                    <li>item1</li>
                                </ul>
                            </div>
                        </div>
                        <div className={[courseContentStyles.courseItem, "bgLightGrey roundBorderDefault"].join(' ')}>
                            <div className={[courseContentStyles.courseItemImgBlock, "roundBorderFull"].join(' ')}></div>
                            <div className={courseContentStyles.courseItemContent}>
                                <h4>Modulo 1</h4>
                                <ul>
                                    <li>item1</li>
                                    <li>item1</li>
                                    <li>item1</li>
                                    <li>item1</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div className={[courseContentStyles.courseExpandBlock, "displayFlex flexFullCenter"].join(' ')}>
                        <button className={["buttonMd", courseContentStyles.expandButton].join(' ')}>mostrar todas as 40 semanas</button>
                    </div>
                    <div className={[courseContentStyles.listLastItens,"displayFlex flexColumn"].join(' ')}>
                        <div className={[courseContentStyles.courseItem, "bgLightGrey roundBorderDefault"].join(' ')}>
                            <div className={[courseContentStyles.courseItemImgBlock, "roundBorderFull"].join(' ')}></div>
                            <div className={courseContentStyles.courseItemContent}>
                                <h4>Modulo 1</h4>
                                <ul>
                                    <li>item1</li>
                                    <li>item1</li>
                                    <li>item1</li>
                                    <li>item1</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default courseContent