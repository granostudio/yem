import React from "react"
import gradientBlockStyles from "./gradientBlock.module.scss"

export default ({ children }) => (
    <div className="greyGradientBg">
        { children }
    </div>
)