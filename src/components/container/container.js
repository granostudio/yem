import React from "react"
import containerStyles from "./container.module.scss"

export default ({ children, internClass, className }) => (
    <div className={[containerStyles.container, internClass, className].join(' ')}>
        {children}
    </div>
)