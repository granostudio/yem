import React from "react"
import startTodayButtonBlockStyles from "./startTodayButtonBlock.module.scss"

class startTodayButtonBlock extends React.Component {
    render() {
        return(
            <div className={[startTodayButtonBlockStyles.startTodayBlock].join(' ')}>
                <p>Comece hoje <span className="textBlack">grátis</span></p>
                <button className="buttonMd buttonDefaultPadding">cadastrar</button>
                <p>cancele quando quiser</p>
            </div>
        )
    }
}
export default startTodayButtonBlock;