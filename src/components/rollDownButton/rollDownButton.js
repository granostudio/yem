import React from "react"

import rollDownButtonStyles from "./rollDownButton.module.scss"

class rollDownButton extends React.Component {
    render() {
        return (
            <div className={rollDownButtonStyles.rollDownButton}>
                <a href={this.props.linkRollDown} className={rollDownButtonStyles.rollDownLink}>
                    <div className={["iconYem arrowDown fontLightGrey", rollDownButtonStyles.rollDownIcon, "displayFlex flexFullCenter"].join(' ')}></div>
                </a>
            </div>
        )
    }
}
export default rollDownButton