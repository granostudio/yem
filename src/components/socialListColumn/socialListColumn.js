import React, { Component } from "react"
import socialListColumnStyles from "./socialListColumn.module.scss"

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebookF, faYoutube, faInstagram } from '@fortawesome/free-brands-svg-icons'

class SocialListColumn extends Component {
    render() {
        return (
            <div className={[socialListColumnStyles.socialListBlock, this.props.className].join(' ')}>
                <a href={this.props.facebookUrl} target="_blank" rel="noopener norefferer" className={["fontLightGrey", socialListColumnStyles.socialListItem].join(' ')}>
                    <FontAwesomeIcon icon={faFacebookF}  />                    
                </a>
                <a href={this.props.youtubeUrl} target="_blank" rel="noopener norefferer" className={["fontLightGrey", socialListColumnStyles.socialListItem].join(' ')}>
                    <FontAwesomeIcon icon={faYoutube}  />
                </a>
                <a href={this.props.instragramUrl} target="_blank" rel="noopener norefferer" className={["fontLightGrey", socialListColumnStyles.socialListItem].join(' ')}>
                    <FontAwesomeIcon icon={faInstagram}  />
                </a>
            </div>
        )
    }
}
export default SocialListColumn;