import React from "react"
import { Link } from "gatsby"
import Container from "../container/container"
import menuStyles from "../menu.module.scss"
import headerSimpleStyles from "../headerSimple/headerSimple.module.scss"

class HeaderSimple extends React.Component {
    render() {
        return (
            <div className="bgDarkGrey">
                <Container>
                    <div className={[menuStyles.menu, headerSimpleStyles.menu].join(' ')}>
                        <Link to="/">
                            <div className={menuStyles.menuLogo}></div>
                        </Link>
                    </div>
                </Container>
            </div>
        )
    }
}
export default HeaderSimple;