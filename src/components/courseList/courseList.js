import React from "react"

import courseListStyles from "./courseList.module.scss"

import courseImage from "../../images/course-item-bg.png"


var courseList = [
    {
        id: 1,
        courseCover: courseImage,
        courseTitle: 'Curso 1',
        courseInstructor: 'Profº Daniel Tonet',
        courseLink: '/'
    },
    {
        id: 2,
        courseCover: courseImage,
        courseTitle: 'Curso 2',
        courseInstructor: 'Profº Daniel Tonet',
        courseLink: '/'
    },
    {
        id: 3,
        courseCover: courseImage,
        courseTitle: 'Curso 3',
        courseInstructor: 'Profº Daniel Tonet',
        courseLink: '/'
    },
    {
        id: 4,
        courseCover: courseImage,
        courseTitle: 'Curso 4',
        courseInstructor: 'Profº Daniel Tonet',
        courseLink: '/'
    }
]


class courseListComponent extends React.Component {
    render() {
        return(
            <div>
                <h3 className={["titleSizeLg alignCenter", this.props.titleclassName, courseListStyles.courseListTitle].join(' ')}>{this.props.blockTitle}</h3>
                <div className={[courseListStyles.courseList, "displayFlex flexWrap"].join(' ')}>

                    {courseList.map(function(i){
                        return(
                            <div key={i.id} className={["mdBlock", courseListStyles.courseItem].join(' ')} style={{backgroundImage: ["url(", i.courseCover,")"].join(' ')}} >
                                <a href={i.courseLink} target="_blank">
                                    <div className={["fullBlock", courseListStyles.courseItemContent].join(' ')}>
                                        <div className={courseListStyles.itemTextBlock}>
                                            <h4>{i.courseTitle}</h4>
                                            <p>{i.courseInstructor}</p>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        )
                    })}

                </div>
            </div>
        )
    }
}
export default courseListComponent