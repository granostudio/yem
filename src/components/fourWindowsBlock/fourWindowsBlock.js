import React from "react"
import { Link } from "gatsby"
import Container from "../container/container"
import fourWindowsBlockStyles from "./fourWindowsBlock.module.scss"
import beginnersImg from "../../images/modalities/beginners.png"
import improvementImg from "../../images/modalities/improvement.png"
import instructorImg from "../../images/modalities/instructor-bg.png"
import onlineImg from "../../images/modalities/online-bg.png"

class fourWindowsBlock extends React.Component {
    render() {
        return (
            <div className="bgLightGrey"> 
                <Container>
                    <div className={fourWindowsBlockStyles.blocksContainer}>
                        <Link to="/" className={["mdBlock", fourWindowsBlockStyles.blocksContainerItem].join(' ')} style={{backgroundImage: ["url(",onlineImg , ")"].join('') }}>
                            <div className={fourWindowsBlockStyles.blockItemContent}>
                                <h2 className={"fontLightGrey"}>Escola online</h2>
                                <p className={"fontLightGrey"}>Centenas de aulas para quem quer inserir o Yoga na sua rotina e conquistar mais produtividade, saúde e vitalidade</p>
                            </div>
                        </Link> 
                        <Link to="/" className={["mdBlock", fourWindowsBlockStyles.blocksContainerItem].join(' ')} style={{backgroundImage: ["url(",instructorImg , ")"].join('') }}>
                            <div className={fourWindowsBlockStyles.blockItemContent}>
                                <h2 className={"fontLightGrey"}>Seja instrutor</h2>
                                <p className={"fontLightGrey"}>Cursos de 200 horas com certificado para você conquistar uma nova profissão e a oportunidade de ensinar e compartilhar a riqueza do Yoga</p>
                            </div> 
                        </Link>
                        <Link to="/" className={["mdBlock", fourWindowsBlockStyles.blocksContainerItem].join(' ')} style={{backgroundImage: ["url(",improvementImg , ")"].join('') }}>
                            <div className={fourWindowsBlockStyles.blockItemContent}>
                                <h2 className={"fontLightGrey"}>Cursos de aprofundamento</h2>
                                <p className={"fontLightGrey"}>Cursos ministrandos pelos mais renomados professores de Yoga para acelerar a evolução na prática</p>
                            </div>
                        </Link>
                        <Link to="/" className={["mdBlock", fourWindowsBlockStyles.blocksContainerItem].join(' ')} style={{backgroundImage: ["url(",beginnersImg , ")"].join('') }}>
                            <div className={fourWindowsBlockStyles.blockItemContent}>
                                <h2 className={"fontLightGrey"}>Curso de iniciantes</h2>
                                <p className={"fontLightGrey"}>Aulas personalizadas para quem busca uma atenção exclusiva e práticas criadas especificadamente para suas necessidades</p>
                            </div>
                        </Link>
                    </div>
                </Container>
            </div>
        );
    }
   
}
export default fourWindowsBlock;