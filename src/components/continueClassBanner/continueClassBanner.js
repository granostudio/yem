import React from "react"
import continueClassBannerStyles from "./continueClassBanner.module.scss"
import imagemTopo from "../../images/student-area-top-bg.png"

import OptionsListColumn from "../../components/optionsListColumn/optionsListColumn"

import Container from "../container/container"

import ProgressBar from "../progressBar/progressBar"



class continueClassBanner extends React.Component {
    render() {
        const completionLevel = this.props.completionLevel;
        const levelInfoText = this.props.levelInfoText;
        const levelName = this.props.levelName;
        return(
            <div className={continueClassBannerStyles.banner} style={{backgroundImage: ["url(",imagemTopo,")"].join('')}}>
                <OptionsListColumn/>
                <Container className={continueClassBannerStyles.bannerContainer}>
                    <div className={continueClassBannerStyles.bannerContent}>
                        <div className={continueClassBannerStyles.bannerTopContent}>
                            <p className={["fontLightGrey", continueClassBannerStyles.bannerTopText].join(' ')}>Práticas com Daniel Tonet</p>
                            <h1 className={["fontLightGrey", continueClassBannerStyles.bannerTopTitle].join(' ')}>Aula de Yôga - 50</h1>
                            <p className={["fontLightGrey", continueClassBannerStyles.bannerTopText].join(' ')}>Linha fina da aula</p>
                        </div>
                        <div className={continueClassBannerStyles.bannerBottomContent}>
                            <a className={["buttonMd", continueClassBannerStyles.bannerBtn].join(' ')}>continuar aula</a>
                            <div className={continueClassBannerStyles.bottomLevel}>
                                <div className={continueClassBannerStyles.levelBadge}>
                                    <p className={continueClassBannerStyles.badgeText}>{completionLevel}</p>
                                </div>
                                <div className={continueClassBannerStyles.bottomLevelInfoBlock}>
                                    <p className={["fontLightGrey textBold", continueClassBannerStyles.infoText].join(' ')}>{levelInfoText}</p>
                                    <p className={["fontLightGrey", continueClassBannerStyles.infoText].join(' ')}>seu nivel: <span className="textBold">{levelName}</span></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </Container>
                <ProgressBar 
                    barWidth={80}
                />
            </div>
        )
    }
}
export default continueClassBanner