import React from "react"
import Slider from "react-slick";

import sliderConfig from "../../styles/sliderConfig.scss"
import YemSliderConfig from "./yemSliderConfig.scss"
import yemSliderStyles from "./yemSlider.module.scss"

import ContainerFluid from "../containerFluid/containerFluid"
import ProgressBar from "../progressBar/progressBar"


 import sliderImg from "../../images/yem-slider-img.png"
 
 class yemSlider extends React.Component {
    render() {
        var settings = {
            dots: false,
            arrows: false,
            infinite: false,
            speed: 500,
            // centerMode: true,
            slidesToShow: 6,
            slidesToScroll: 6,
            className: "simpleSlider",
            responsive: [
                {
                  breakpoint: 1024,
                  settings: {
                    slidesToShow: 4,
                    slidesToScroll: 4,
                  }
                },
                {
                  breakpoint: 680,
                  settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    initialSlide: 1,
                    centerMode: true,
                  }
                },
              ]
        };
        var titleClassName = this.props.titleClassName;
        var titleText = this.props.titleText;
        return(
            <div>
                <ContainerFluid>
                    <h3 className={["fontLightGrey", yemSliderStyles.sliderTitle, titleClassName].join(' ')}>{titleText}</h3>
                </ContainerFluid>
                <div id="yemSlider">
                    <Slider {...settings}>
                        <div className={yemSliderStyles.sliderItemContent}>
                            <div className={yemSliderStyles.sliderItem} >
                                <div className={yemSliderStyles.itemInfoBlock}>
                                    <div className={yemSliderStyles.classInfoTop}>
                                        <div>
                                            <p className={["fontPurple displayFlex", yemSliderStyles.classText, yemSliderStyles.classTime].join(' ')}> <span className={["iconYem clockSimple fontPurple", yemSliderStyles.timeIcon].join(' ')}></span> 2:30</p>
                                        </div>
                                        <div>
                                            <p className={["displayFlex fontPurple", yemSliderStyles.classText].join(' ')}>Prof. Daniel Tonet</p>
                                        </div>
                                    </div>  
                                    <div className={yemSliderStyles.classInfoBottom}>
                                        <div className={yemSliderStyles.itemInfo}>
                                            <p className={["fontLightGrey textBlack", yemSliderStyles.infoText].join(" ")}>Aula nome</p>
                                            <p className={["fontLightGrey", yemSliderStyles.infoText].join(" ")}>Linha fina da aula</p>
                                        </div>
                                        <div className={yemSliderStyles.itemOptions}>
                                            <div className={["iconYem arrowDown fontLightGrey", yemSliderStyles.itemIcon, yemSliderStyles.itemArrowIcon ].join(' ')}></div>
                                            <div className={[yemSliderStyles.itemOptionsList, "itemOptionsList"].join(' ')}>
                                                <div className={[yemSliderStyles.itemOptionBlock, "itemOptionBlock"].join(' ')}>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem paperPlane", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem star", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem heart", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className={yemSliderStyles.itemCover} style={{backgroundImage: ["url(", sliderImg, ")"].join(' ')}}>
                                <ProgressBar 
                                    barWidth={42}
                                />
                                </div>
                            </div>
                        </div>
                        <div className={yemSliderStyles.sliderItemContent}>
                            <div className={yemSliderStyles.sliderItem} >
                                <div className={yemSliderStyles.itemInfoBlock}>
                                    <div className={yemSliderStyles.classInfoTop}>
                                        <div>
                                            <p className={["fontPurple displayFlex", yemSliderStyles.classText, yemSliderStyles.classTime].join(' ')}> <span className={["iconYem clockSimple fontPurple", yemSliderStyles.timeIcon].join(' ')}></span> 2:30</p>
                                        </div>
                                        <div>
                                            <p className={["displayFlex fontPurple", yemSliderStyles.classText].join(' ')}>Prof. Daniel Tonet</p>
                                        </div>
                                    </div>  
                                    <div className={yemSliderStyles.classInfoBottom}>
                                        <div className={yemSliderStyles.itemInfo}>
                                            <p className={["fontLightGrey textBlack", yemSliderStyles.infoText].join(" ")}>Aula nome</p>
                                            <p className={["fontLightGrey", yemSliderStyles.infoText].join(" ")}>Linha fina da aula</p>
                                        </div>
                                        <div className={yemSliderStyles.itemOptions}>
                                            <div className={["iconYem arrowDown fontLightGrey", yemSliderStyles.itemIcon, yemSliderStyles.itemArrowIcon ].join(' ')}></div>
                                            <div className={[yemSliderStyles.itemOptionsList, "itemOptionsList"].join(' ')}>
                                                <div className={[yemSliderStyles.itemOptionBlock, "itemOptionBlock"].join(' ')}>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem paperPlane", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem star", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem heart", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className={yemSliderStyles.itemCover} style={{backgroundImage: ["url(", sliderImg, ")"].join(' ')}}>
                                <ProgressBar 
                                    barWidth={10}
                                />
                                </div>
                            </div>
                        </div>
                        <div className={yemSliderStyles.sliderItemContent}>
                            <div className={yemSliderStyles.sliderItem} >
                                <div className={yemSliderStyles.itemInfoBlock}>
                                    <div className={yemSliderStyles.classInfoTop}>
                                        <div>
                                            <p className={["fontPurple displayFlex", yemSliderStyles.classText, yemSliderStyles.classTime].join(' ')}> <span className={["iconYem clockSimple fontPurple", yemSliderStyles.timeIcon].join(' ')}></span> 2:30</p>
                                        </div>
                                        <div>
                                            <p className={["displayFlex fontPurple", yemSliderStyles.classText].join(' ')}>Prof. Daniel Tonet</p>
                                        </div>
                                    </div>  
                                    <div className={yemSliderStyles.classInfoBottom}>
                                        <div className={yemSliderStyles.itemInfo}>
                                            <p className={["fontLightGrey textBlack", yemSliderStyles.infoText].join(" ")}>Aula nome</p>
                                            <p className={["fontLightGrey", yemSliderStyles.infoText].join(" ")}>Linha fina da aula</p>
                                        </div>
                                        <div className={yemSliderStyles.itemOptions}>
                                            <div className={["iconYem arrowDown fontLightGrey", yemSliderStyles.itemIcon, yemSliderStyles.itemArrowIcon ].join(' ')}></div>
                                            <div className={[yemSliderStyles.itemOptionsList, "itemOptionsList"].join(' ')}>
                                                <div className={[yemSliderStyles.itemOptionBlock, "itemOptionBlock"].join(' ')}>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem paperPlane", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem star", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem heart", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className={yemSliderStyles.itemCover} style={{backgroundImage: ["url(", sliderImg, ")"].join(' ')}}>
                                <ProgressBar 
                                    barWidth={83}
                                />
                                </div>
                            </div>
                        </div>
                        <div className={yemSliderStyles.sliderItemContent}>
                            <div className={yemSliderStyles.sliderItem} >
                                <div className={yemSliderStyles.itemInfoBlock}>
                                    <div className={yemSliderStyles.classInfoTop}>
                                        <div>
                                            <p className={["fontPurple displayFlex", yemSliderStyles.classText, yemSliderStyles.classTime].join(' ')}> <span className={["iconYem clockSimple fontPurple", yemSliderStyles.timeIcon].join(' ')}></span> 2:30</p>
                                        </div>
                                        <div>
                                            <p className={["displayFlex fontPurple", yemSliderStyles.classText].join(' ')}>Prof. Daniel Tonet</p>
                                        </div>
                                    </div>  
                                    <div className={yemSliderStyles.classInfoBottom}>
                                        <div className={yemSliderStyles.itemInfo}>
                                            <p className={["fontLightGrey textBlack", yemSliderStyles.infoText].join(" ")}>Aula nome</p>
                                            <p className={["fontLightGrey", yemSliderStyles.infoText].join(" ")}>Linha fina da aula</p>
                                        </div>
                                        <div className={yemSliderStyles.itemOptions}>
                                            <div className={["iconYem arrowDown fontLightGrey", yemSliderStyles.itemIcon, yemSliderStyles.itemArrowIcon ].join(' ')}></div>
                                            <div className={[yemSliderStyles.itemOptionsList, "itemOptionsList"].join(' ')}>
                                                <div className={[yemSliderStyles.itemOptionBlock, "itemOptionBlock"].join(' ')}>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem paperPlane", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem star", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem heart", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className={yemSliderStyles.itemCover} style={{backgroundImage: ["url(", sliderImg, ")"].join(' ')}}>
                                <ProgressBar 
                                    barWidth={25}
                                />
                                </div>
                            </div>
                        </div>
                        <div className={yemSliderStyles.sliderItemContent}>
                            <div className={yemSliderStyles.sliderItem} >
                                <div className={yemSliderStyles.itemInfoBlock}>
                                    <div className={yemSliderStyles.classInfoTop}>
                                        <div>
                                            <p className={["fontPurple displayFlex", yemSliderStyles.classText, yemSliderStyles.classTime].join(' ')}> <span className={["iconYem clockSimple fontPurple", yemSliderStyles.timeIcon].join(' ')}></span> 2:30</p>
                                        </div>
                                        <div>
                                            <p className={["displayFlex fontPurple", yemSliderStyles.classText].join(' ')}>Prof. Daniel Tonet</p>
                                        </div>
                                    </div>  
                                    <div className={yemSliderStyles.classInfoBottom}>
                                        <div className={yemSliderStyles.itemInfo}>
                                            <p className={["fontLightGrey textBlack", yemSliderStyles.infoText].join(" ")}>Aula nome</p>
                                            <p className={["fontLightGrey", yemSliderStyles.infoText].join(" ")}>Linha fina da aula</p>
                                        </div>
                                        <div className={yemSliderStyles.itemOptions}>
                                            <div className={["iconYem arrowDown fontLightGrey", yemSliderStyles.itemIcon, yemSliderStyles.itemArrowIcon ].join(' ')}></div>
                                            <div className={[yemSliderStyles.itemOptionsList, "itemOptionsList"].join(' ')}>
                                                <div className={[yemSliderStyles.itemOptionBlock, "itemOptionBlock"].join(' ')}>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem paperPlane", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem star", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem heart", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className={yemSliderStyles.itemCover} style={{backgroundImage: ["url(", sliderImg, ")"].join(' ')}}>
                                <ProgressBar 
                                    barWidth={67}
                                />
                                </div>
                            </div>
                        </div>
                        <div className={yemSliderStyles.sliderItemContent}>
                            <div className={yemSliderStyles.sliderItem} >
                                <div className={yemSliderStyles.itemInfoBlock}>
                                    <div className={yemSliderStyles.classInfoTop}>
                                        <div>
                                            <p className={["fontPurple displayFlex", yemSliderStyles.classText, yemSliderStyles.classTime].join(' ')}> <span className={["iconYem clockSimple fontPurple", yemSliderStyles.timeIcon].join(' ')}></span> 2:30</p>
                                        </div>
                                        <div>
                                            <p className={["displayFlex fontPurple", yemSliderStyles.classText].join(' ')}>Prof. Daniel Tonet</p>
                                        </div>
                                    </div>  
                                    <div className={yemSliderStyles.classInfoBottom}>
                                        <div className={yemSliderStyles.itemInfo}>
                                            <p className={["fontLightGrey textBlack", yemSliderStyles.infoText].join(" ")}>Aula nome</p>
                                            <p className={["fontLightGrey", yemSliderStyles.infoText].join(" ")}>Linha fina da aula</p>
                                        </div>
                                        <div className={yemSliderStyles.itemOptions}>
                                            <div className={["iconYem arrowDown fontLightGrey", yemSliderStyles.itemIcon, yemSliderStyles.itemArrowIcon ].join(' ')}></div>
                                            <div className={[yemSliderStyles.itemOptionsList, "itemOptionsList"].join(' ')}>
                                                <div className={[yemSliderStyles.itemOptionBlock, "itemOptionBlock"].join(' ')}>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem paperPlane", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem star", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem heart", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className={yemSliderStyles.itemCover} style={{backgroundImage: ["url(", sliderImg, ")"].join(' ')}}>
                                <ProgressBar 
                                    barWidth={95}
                                />
                                </div>
                            </div>
                        </div>
                        <div className={yemSliderStyles.sliderItemContent}>
                            <div className={yemSliderStyles.sliderItem} >
                                <div className={yemSliderStyles.itemInfoBlock}>
                                    <div className={yemSliderStyles.classInfoTop}>
                                        <div>
                                            <p className={["fontPurple displayFlex", yemSliderStyles.classText, yemSliderStyles.classTime].join(' ')}> <span className={["iconYem clockSimple fontPurple", yemSliderStyles.timeIcon].join(' ')}></span> 2:30</p>
                                        </div>
                                        <div>
                                            <p className={["displayFlex fontPurple", yemSliderStyles.classText].join(' ')}>Prof. Daniel Tonet</p>
                                        </div>
                                    </div>  
                                    <div className={yemSliderStyles.classInfoBottom}>
                                        <div className={yemSliderStyles.itemInfo}>
                                            <p className={["fontLightGrey textBlack", yemSliderStyles.infoText].join(" ")}>Aula nome</p>
                                            <p className={["fontLightGrey", yemSliderStyles.infoText].join(" ")}>Linha fina da aula</p>
                                        </div>
                                        <div className={yemSliderStyles.itemOptions}>
                                            <div className={["iconYem arrowDown fontLightGrey", yemSliderStyles.itemIcon, yemSliderStyles.itemArrowIcon ].join(' ')}></div>
                                            <div className={[yemSliderStyles.itemOptionsList, "itemOptionsList"].join(' ')}>
                                                <div className={[yemSliderStyles.itemOptionBlock, "itemOptionBlock"].join(' ')}>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem paperPlane", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem star", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem heart", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className={yemSliderStyles.itemCover} style={{backgroundImage: ["url(", sliderImg, ")"].join(' ')}}>
                                <ProgressBar 
                                    barWidth={42}
                                />
                                </div>
                            </div>
                        </div>
                        <div className={yemSliderStyles.sliderItemContent}>
                            <div className={yemSliderStyles.sliderItem} >
                                <div className={yemSliderStyles.itemInfoBlock}>
                                    <div className={yemSliderStyles.classInfoTop}>
                                        <div>
                                            <p className={["fontPurple displayFlex", yemSliderStyles.classText, yemSliderStyles.classTime].join(' ')}> <span className={["iconYem clockSimple fontPurple", yemSliderStyles.timeIcon].join(' ')}></span> 2:30</p>
                                        </div>
                                        <div>
                                            <p className={["displayFlex fontPurple", yemSliderStyles.classText].join(' ')}>Prof. Daniel Tonet</p>
                                        </div>
                                    </div>  
                                    <div className={yemSliderStyles.classInfoBottom}>
                                        <div className={yemSliderStyles.itemInfo}>
                                            <p className={["fontLightGrey textBlack", yemSliderStyles.infoText].join(" ")}>Aula nome</p>
                                            <p className={["fontLightGrey", yemSliderStyles.infoText].join(" ")}>Linha fina da aula</p>
                                        </div>
                                        <div className={yemSliderStyles.itemOptions}>
                                            <div className={["iconYem arrowDown fontLightGrey", yemSliderStyles.itemIcon, yemSliderStyles.itemArrowIcon ].join(' ')}></div>
                                            <div className={[yemSliderStyles.itemOptionsList, "itemOptionsList"].join(' ')}>
                                                <div className={[yemSliderStyles.itemOptionBlock, "itemOptionBlock"].join(' ')}>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem paperPlane", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem star", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem heart", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className={yemSliderStyles.itemCover} style={{backgroundImage: ["url(", sliderImg, ")"].join(' ')}}>
                                <ProgressBar 
                                    barWidth={100}
                                />
                                </div>
                            </div>
                        </div>
                        <div className={yemSliderStyles.sliderItemContent}>
                            <div className={yemSliderStyles.sliderItem} >
                                <div className={yemSliderStyles.itemInfoBlock}>
                                    <div className={yemSliderStyles.classInfoTop}>
                                        <div>
                                            <p className={["fontPurple displayFlex", yemSliderStyles.classText, yemSliderStyles.classTime].join(' ')}> <span className={["iconYem clockSimple fontPurple", yemSliderStyles.timeIcon].join(' ')}></span> 2:30</p>
                                        </div>
                                        <div>
                                            <p className={["displayFlex fontPurple", yemSliderStyles.classText].join(' ')}>Prof. Daniel Tonet</p>
                                        </div>
                                    </div>  
                                    <div className={yemSliderStyles.classInfoBottom}>
                                        <div className={yemSliderStyles.itemInfo}>
                                            <p className={["fontLightGrey textBlack", yemSliderStyles.infoText].join(" ")}>Aula nome</p>
                                            <p className={["fontLightGrey", yemSliderStyles.infoText].join(" ")}>Linha fina da aula</p>
                                        </div>
                                        <div className={yemSliderStyles.itemOptions}>
                                            <div className={["iconYem arrowDown fontLightGrey", yemSliderStyles.itemIcon, yemSliderStyles.itemArrowIcon ].join(' ')}></div>
                                            <div className={[yemSliderStyles.itemOptionsList, "itemOptionsList"].join(' ')}>
                                                <div className={[yemSliderStyles.itemOptionBlock, "itemOptionBlock"].join(' ')}>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem paperPlane", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem star", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                    <div className={yemSliderStyles.item}>
                                                        <div className={["iconYem heart", yemSliderStyles.itemIcon].join(' ')}></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className={yemSliderStyles.itemCover} style={{backgroundImage: ["url(", sliderImg, ")"].join(' ')}}>
                                <ProgressBar 
                                    barWidth={8}
                                />
                                </div>
                            </div>
                        </div>
                    </Slider>
                </div>
            </div>
        )
    }
 }

 export default yemSlider