import React from "react"
import switchStyles from "./switchInput.module.scss"

class switchInput extends React.Component {
    render(){
        const className = this.props.className;
        const textContainerClass = this.props.textContainerClass;
        const switchText = this.props.switchText;
        return(
            <div className={["displayFlex flexFullCenter", className].join(' ')}>
                <div className={[switchStyles.textContainer, "displayFlex flexAlignCenter", textContainerClass].join(' ')}>
                    <p className={switchStyles.text}>{switchText}</p>
                </div>
                <label className={switchStyles.switch}>
                        <input type="checkbox" className={switchStyles.input} />
                        <span className={[switchStyles.slider, switchStyles.round].join(' ')}></span>
                </label>
            </div>
        )
    }
}
export default switchInput