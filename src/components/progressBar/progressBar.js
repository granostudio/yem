import React from "react"
import progressBarStyles from "./progressBar.module.scss"

class progressBar extends React.Component {
    render() {
        var barWidth = this.props.barWidth;
        return(
            <div className={progressBarStyles.progressBar} style={{width: [barWidth,"%"].join('')}}>

            </div>
        )
    }
}
export default progressBar