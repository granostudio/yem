import React from "react"
import containerfluidStyles from "./containerFluid.module.scss"

export default ({ children, className }) => (
    <div className={[containerfluidStyles.containerFluid, className].join(' ')}>
        {children}
    </div>
)