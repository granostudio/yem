import React from "react"
import Container from "../container/container"
import textCircleImgBlockStyles from "../textCircleImgBlock/textCircleImgBlock.module.scss"

class textCircleImgBlock extends React.Component {
    render() {
        return(
            <div className={textCircleImgBlockStyles.textCircleBlock}>
                <Container internClass="displayFlex flexWrap">
                    <div className="mdBlock">
                        <h2 className={textCircleImgBlockStyles.benefitTitle}>A escola que vai com você onde você estiver</h2>
                        <div className={textCircleImgBlockStyles.benefitBlockList}>
                            <div className={textCircleImgBlockStyles.benefitBlockItem}>
                                <div className="iconYem iconXl clockSimple"></div>
                                <div className={textCircleImgBlockStyles.benefitItemTextBlock}>
                                    <p>Aulas em qualquer horário, em qualquer dia da semana, quantas vezes você quiser</p>
                                </div>
                            </div>
                            <div className={textCircleImgBlockStyles.benefitBlockItem}>
                                <div className="iconYem iconXl diamond"></div>
                                <div className={textCircleImgBlockStyles.benefitItemTextBlock}>
                                    <p>Aulas práticas e teóricas, treinamentos de meditação, ásanas e outros</p>
                                </div>
                            </div>
                            <div className={textCircleImgBlockStyles.benefitBlockItem}>
                                <div className="iconYem iconXl comment"></div>
                                <div className={textCircleImgBlockStyles.benefitItemTextBlock}>
                                    <p>Encontros semanais ao vivo por video conferência para tirar todas as suas dúvidas</p>
                                </div>
                            </div>
                            <div className={textCircleImgBlockStyles.benefitBlockItem}>
                                <div className="iconYem iconXl globe"></div>
                                <div className={textCircleImgBlockStyles.benefitItemTextBlock}>
                                    <p>A combinação perfeita entre o mundo moderno e as técnicas ancestrais do Yoga para que você possa criar a sua melhor versão  </p>
                                </div>
                            </div>
                        </div>
                        <div className={textCircleImgBlockStyles.benefitPrice}>
                            <p className={["textBlack"].join(' ')}>Apenas R$ 47/mês</p>
                            <p className={textCircleImgBlockStyles.benefitPriceText}>você tem acesso a todo conteúdo, disponível em nosso portal</p>
                            <button className="buttonMd">comece grátis</button>
                        </div>
                    </div>
                    <div className="mdBlock">
                        <div className={textCircleImgBlockStyles.benefitImageBlock}>
                            <img src={this.props.benefitImg} className={textCircleImgBlockStyles.benefitImageItem}></img>
                        </div>
                    </div>
                </Container>
            </div>
        );
    }
}
export default textCircleImgBlock;