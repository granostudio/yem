import React, { Component } from "react"
import optionsListColumnStyles from "./optionsListColumn.module.scss"

class SocialListColumn extends Component {
    render() {
        return (
            <div className={[optionsListColumnStyles.socialListBlock, this.props.className].join(' ')}>
                <a href={this.props.facebookUrl} target="_blank" rel="noopener norefferer" className={["iconYem paperPlane fontLightGrey", optionsListColumnStyles.socialListItem].join(' ')}>
                    
                </a>
                <div className={["iconYem star fontLightGrey", optionsListColumnStyles.socialListItem].join(' ')}>
                    
                </div>
                <div className={["iconYem heart fontLightGrey", optionsListColumnStyles.socialListItem].join(' ')}>
                    
                </div>
            </div>
        )
    }
}
export default SocialListColumn;