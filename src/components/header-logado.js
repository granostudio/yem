import React from "react"
import MenuLogado from "./menu-logado"
import SearchBar from "./searchBar/searchBar"
import { Link } from "gatsby"
import headerStyles from "./header.module.scss"

export default () => (
    <div className={headerStyles.menuContainer}>
        <MenuLogado>
            <SearchBar />
        </MenuLogado>
    </div>
    
)

        if (typeof window !== "undefined") {
        // eslint-disable-next-line global-require
        require("smooth-scroll")('a[href*="#"]',{
            speed: 2000
        })
        }
        