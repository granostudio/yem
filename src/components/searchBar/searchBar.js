import React from "react"
import searchBarStyles from "./searchBar.module.scss"
import searchBarSCSS from "./searchBar.scss"
import $ from "jquery"


class searchBar extends React.Component {
    componentDidMount() {
        $(document).ready(function(){
            // console.log("teste");
            var searchButton = $('.search');
            var searchBarInput = $('#searchBarInput');

            function searchBarResponsive() {
                searchButton.click(function() {
                    searchBarInput.animate({width: '100%', opacity: '1'});
                    searchBarInput.attr("placeholder", " ");
                })
            }
            
            if( $(this).width() < 680 ) {
                searchBarResponsive()
            }

            $(window).resize(function() {
                if( $(this).width() < 680 ) {
                    searchBarResponsive()
                }
            });

        });
    }
    render(){
        return(
            <div className={searchBarStyles.searchBarContent}>
                <form>
                    <div className={searchBarStyles.searchBar}>
                        <label className={["iconYem search"].join(' ')} htmlFor="searchBarInput"></label>
                        <input type="search" id="searchBarInput" placeholder="busque por uma aula"></input>
                    </div>
                </form>
            </div>
        )
    }
}
export default searchBar;