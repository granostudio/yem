import React from "react"
import imageTextIntroStyles from "./imageTextIntro.module.scss"
import Container from "../container/container"
import yogaWoman from "../../images/text-intro-bg.png"

export default ( {children, containerClass, titleClass, titleText, infoTextClass, infoText, btnClass, btnText } ) => (
    <div className={imageTextIntroStyles.imageTextIntro} style={{backgroundImage: ["url(", yogaWoman, ")"].join('') }}>
        <div className={[imageTextIntroStyles.imageTextContainer, containerClass ].join(' ')}>
            <Container>
                <div className={imageTextIntroStyles.imageTextInfoBlock}>
                    <h1 className={["fontWhite textUpper", imageTextIntroStyles.textInfoTitle, titleClass ].join(' ')}>{titleText || "o yoga mais perto de você"}</h1>
                    <p className={["fontWhite", imageTextIntroStyles.textInfoText, infoTextClass].join(' ')}> {infoText || "o mundo do Yoga ao seu alcance" } </p>
                    <button className={["buttonMd", imageTextIntroStyles.textInfoBtn, btnClass].join(' ')}> {btnText || "comece grátis" } </button>
                </div>
            </Container>
        </div>
        {children}
    </div>
)