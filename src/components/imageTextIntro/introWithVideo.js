import React from "react"
import Container from "../container/container"
import introWithVideoStyles from "./introWithVideo.module.scss"

import Modal from 'react-responsive-modal';

class introWithVideo extends React.Component {

    state = {
        open: false,
    };
    
    onOpenModal = () => {
        this.setState({ open: true });
    };
    
    onCloseModal = () => {
        this.setState({ open: false });
    };

    render(){
        const { open } = this.state;
        return(
            <div className={introWithVideoStyles.introVideoBlock} style={{backgroundImage: ["url(",this.props.introImg ,")"].join(' ')}}>
                <div className={introWithVideoStyles.introTextBlock}>
                    <Container>
                        <div className={["alignCenter",this.props.contentBlockCustomClass].join(' ')}>
                            <div className={["fontLightGrey iconYem", this.props.iconClass,introWithVideoStyles.introIcon].join(' ')} onClick={this.onOpenModal} ></div>
                            <h1 className={["fontLightGrey textUpper", this.props.titleCustomClass].join(' ')}>{this.props.introTitle}</h1>
                            <p className={["fontLightGrey titleSizeSm", this.props.firstTextCustomClass].join(' ')}>{this.props.firstIntroText}</p>
                            <p className={["fontLightGrey titleSizeSm", this.props.secondTextCustomClass].join(' ')}>{this.props.secondIntroText}</p>
                        </div>
                    </Container>
                </div>
                {this.props.children}
                <Modal open={open} onClose={this.onCloseModal} 
                    classNames={ {
                        overlay: introWithVideoStyles.overlayModal,
                        closeButton: introWithVideoStyles.closeButton,
                        closeIcon: introWithVideoStyles.closeButtonSvg,
                        modal: introWithVideoStyles.modalVideo
                    }} 
                    center
                >
                        <iframe width="560" height="315" src="https://www.youtube.com/embed/UV9ULBdImg0" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </Modal>
            </div>
        )
    }
}

export default introWithVideo