import React from "react"
import Container from "../container/container"
import testimonialsStyles from "./testimonials.module.scss"

export default () => (
    <div className="darkGreyBg">
        <Container>
            <div className={testimonialsStyles.testimonialBlock}>
                <div className={["fullBlock", testimonialsStyles.testimonialLgBlock].join(' ')}>
                    <div className={testimonialsStyles.testimonialBlockList}>
                        <div className={[testimonialsStyles.testimonialItem, testimonialsStyles.testimonialSmallBlockItem].join(' ')}>
                            <div className={testimonialsStyles.testimonialBlockItem}>
                                <div className={testimonialsStyles.testimonialItemImg}></div>
                                <div className={testimonialsStyles.testimonialItemTextBlock}>
                                    <p className={testimonialsStyles.testimonialItemPhrase}>O Yôga em movimento é 10! O istrutores são ótimos! Obrigado por toda atenção!</p>
                                    <p className={testimonialsStyles.testimonialItemAuthor}>Fernanda Lima</p>
                                </div>
                            </div>
                            <div className={testimonialsStyles.testimonialBlockItem}>
                                <div className={testimonialsStyles.testimonialItemImg}></div>
                                <div className={testimonialsStyles.testimonialItemTextBlock}>
                                    <p className={testimonialsStyles.testimonialItemPhrase}>O Yôga em movimento é 10! O istrutores são ótimos! Obrigado por toda atenção!</p>
                                    <p className={testimonialsStyles.testimonialItemAuthor}>Fernanda Lima</p>
                                </div>
                            </div>
                        </div>
                        <div className={[testimonialsStyles.testimonialItem, testimonialsStyles.testimonialSmallBlockItem].join(' ')}>
                            <div className={testimonialsStyles.testimonialBlockItem}>
                                <div className={testimonialsStyles.testimonialItemImg}></div>
                                <div className={testimonialsStyles.testimonialItemTextBlock}>
                                    <p className={testimonialsStyles.testimonialItemPhrase}>O Yôga em movimento é 10! O istrutores são ótimos! Obrigado por toda atenção!</p>
                                    <p className={testimonialsStyles.testimonialItemAuthor}>Fernanda Lima</p>
                                </div>
                            </div>
                            <div className={testimonialsStyles.testimonialBlockItem}>
                                <div className={testimonialsStyles.testimonialItemImg}></div>
                                <div className={testimonialsStyles.testimonialItemTextBlock}>
                                    <p className={testimonialsStyles.testimonialItemPhrase}>O Yôga em movimento é 10! O istrutores são ótimos! Obrigado por toda atenção!</p>
                                    <p className={testimonialsStyles.testimonialItemAuthor}>Fernanda Lima</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </Container>
    </div>
)