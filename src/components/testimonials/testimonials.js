import React from "react"
import Container from "../container/container"
import testimonialsStyles from "./testimonials.module.scss"

export default () => (
    <div className="darkGreyBg">
        <Container>
            <div className={testimonialsStyles.testimonialBlock}>
                <div className={["smBlock",testimonialsStyles.testimonialSmBlock].join(' ')}>
                    <div className={testimonialsStyles.infoBlock}>
                        <h2 className={[testimonialsStyles.infoTitle, 'fontLightGrey'].join(' ')}>O que os nossos alunos dizem</h2>
                        <div className={[testimonialsStyles.startTodayBlock].join(' ')}>
                            <p className={[testimonialsStyles.infoText, 'fontLightGrey'].join(' ')}>Comece hoje <span className="textBold">grátis</span></p>
                            <button className={["buttonMd",testimonialsStyles.infoBtn ].join(' ')}>cadastrar</button>
                            <p className={['fontLightGrey', testimonialsStyles.infoSmallText].join(' ')}>cancele quando quiser</p>
                        </div>
                    </div>
                </div>
                <div className={["lgBlock", testimonialsStyles.testimonialLgBlock].join(' ')}>
                    <div className={testimonialsStyles.testimonialBlockList}>
                        <div className={[testimonialsStyles.testimonialItem, testimonialsStyles.testimonialSmallBlockItem].join(' ')}>
                            <div className={testimonialsStyles.testimonialBlockItem}>
                                <div className={testimonialsStyles.testimonialItemImg}></div>
                                <div className={testimonialsStyles.testimonialItemTextBlock}>
                                    <p className={testimonialsStyles.testimonialItemPhrase}>O Yôga em movimento é 10! O istrutores são ótimos! Obrigado por toda atenção!</p>
                                    <p className={testimonialsStyles.testimonialItemAuthor}>Fernanda Lima</p>
                                </div>
                            </div>
                            <div className={testimonialsStyles.testimonialBlockItem}>
                                <div className={testimonialsStyles.testimonialItemImg}></div>
                                <div className={testimonialsStyles.testimonialItemTextBlock}>
                                    <p className={testimonialsStyles.testimonialItemPhrase}>O Yôga em movimento é 10! O istrutores são ótimos! Obrigado por toda atenção!</p>
                                    <p className={testimonialsStyles.testimonialItemAuthor}>Fernanda Lima</p>
                                </div>
                            </div>
                        </div>
                        <div className={[testimonialsStyles.testimonialItem, testimonialsStyles.testimonialLargeBlockItem].join(' ')}>
                            <div className={testimonialsStyles.testimonialBlockItem}>
                                <div className={testimonialsStyles.testimonialItemImg}></div>
                                <div className={testimonialsStyles.testimonialItemTextBlock}>
                                    <p className={testimonialsStyles.testimonialItemPhrase}>O Yôga em movimento é 10! O istrutores são ótimos! Obrigado por toda atenção!</p>
                                    <p className={testimonialsStyles.testimonialItemAuthor}>Fernanda Lima</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className={testimonialsStyles.testimonialSeeMoreBlock}>
                        <div className={testimonialsStyles.seeMoreBtnBlock}>
                            <button className={testimonialsStyles.seeMoreBtn}>
                                <div className={"iconYem iconXXl arrowDown"}></div>
                                <p>Ver mais comentarios</p>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </Container>
    </div>
)