import React from "react"
import Container from "../../components/container/container"
import threeCircleBlockStyles from "./threeCircleBlock.module.scss"

class threeCircleBlock extends React.Component {
    render() {
        return(
            <div>
                <div className={threeCircleBlockStyles.topBlock} style={{backgroundImage: ["url(", this.props.topBgImage, ")"].join('') }}>
                    <Container>
                        <div className={threeCircleBlockStyles.topBlockContent}>
                            <h2 className={["fontLightGrey",threeCircleBlockStyles.topBlockTitle].join(' ')}>{this.props.topTitle}</h2>
                            <p className={["fontLightGrey"].join(' ')}>{this.props.topText1}</p>
                            <p className={["fontLightGrey"].join(' ')}>{this.props.topText2}</p>
                            <p className={["fontLightGrey"].join(' ')}>{this.props.topText3}</p>
                            <p className={["fontLightGrey"].join(' ')}>{this.props.topText4}</p>
                        </div>
                    </Container>
                </div>
                <div className={[threeCircleBlockStyles.infoBlock, "lightGreyBg"].join(' ')}>
                    <Container>
                        <div className={[threeCircleBlockStyles.infoBlockList, "displayFlex"].join(' ')}>
                            <div className={[threeCircleBlockStyles.infoListItem, "smBlock lightGreyBg roundBorderDefault"].join(' ')}>
                                <div className={[threeCircleBlockStyles.infoItemIcon, "displayFlex flexFullCenter lightGreyBg roundBorderFull"].join(' ')}>
                                    <div className={["iconYem clockSimple iconXXl fontDarkGrey", threeCircleBlockStyles.icon].join(' ')}></div>
                                </div>
                                <div className={[threeCircleBlockStyles.infoItemContent, "displayFlex flexColumn flexFullCenter"].join(' ')}>
                                    <h3 className={["fontBlack alignCenter", threeCircleBlockStyles.itemContentTitle].join(' ')}>Duração</h3>
                                    <p className={["alignCenter", threeCircleBlockStyles.itemContentText].join(' ')}>Aulas de 15, 45 e 90 min.
                                        Para se adaptarem perfeitamente a sua rotina</p>
                                </div>
                            </div>
                            <div className={[threeCircleBlockStyles.infoListItem, "smBlock lightGreyBg roundBorderDefault"].join(' ')}>
                                <div className={[threeCircleBlockStyles.infoItemIcon, "displayFlex flexFullCenter lightGreyBg roundBorderFull"].join(' ')}>
                                    <div className={["iconYem params iconXXl fontDarkGrey", threeCircleBlockStyles.icon].join(' ')}></div>
                                </div>
                                <div className={[threeCircleBlockStyles.infoItemContent, "displayFlex flexColumn flexFullCenter"].join(' ')}>
                                    <h3 className={["fontBlack alignCenter", threeCircleBlockStyles.itemContentTitle].join(' ')}>Dificuldade</h3>
                                    <p className={["alignCenter", threeCircleBlockStyles.itemContentText].join(' ')}>Escolha entre níveis: Iniciante, intermediário e avançado</p>
                                </div>
                            </div>
                            <div className={[threeCircleBlockStyles.infoListItem, "smBlock lightGreyBg roundBorderDefault"].join(' ')}>
                                <div className={[threeCircleBlockStyles.infoItemIcon, "displayFlex flexFullCenter lightGreyBg roundBorderFull"].join(' ')}>
                                    <div className={["iconYem heart iconXXl fontDarkGrey", threeCircleBlockStyles.icon].join(' ')}></div>
                                </div>
                                <div className={[threeCircleBlockStyles.infoItemContent, "displayFlex flexColumn flexFullCenter"].join(' ')}>
                                    <h3 className={["fontBlack alignCenter", threeCircleBlockStyles.itemContentTitle].join(' ')}>Professor</h3>
                                    <p className={["alignCenter", threeCircleBlockStyles.itemContentText].join(' ')}>Pratique com todos os professores ou acompanhe o seu preferido</p>
                                </div>
                            </div>
                        </div>
                    </Container>
                </div>
            </div>
        )
    }
}
export default threeCircleBlock;