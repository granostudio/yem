import React, { Component } from "react"
import { Link } from "gatsby"
import classNames from 'classnames';
import menuStyles from "./menu.module.scss"

import InboxAlert from "./inboxAlert/inboxAlert"


class MenuComponent extends Component { 
    state = {
        isActive: false
    };

    handleClick = () => {
        this.setState(state => ({ isActive: !state.isActive }));
    };

    render() {
        const menuClass = classNames({
          menu: true,
          menuActive: this.state.isActive
        });

        const children = this.props.children
    
        return (
        <div className={menuStyles.menu}>
            <Link to="/">
                <div className={menuStyles.menuLogo}></div>
            </Link>
            <div className={menuStyles.optionsList}>
                {children}
                <div className={menuStyles.menuBtnBlock}>
                    <InboxAlert 
                        inboxAlertCount={1}
                    />
                    <div className={menuStyles.menuLinksBlock}>
                        <div className={[menuStyles.menuLinksItem, menuStyles.menuLinksItemHalfActive].join(' ')} onClick={this.handleClick}>menu</div>
                    </div>
                    <div className={[menuStyles.menuButton, menuClass].join(' ')} onClick={this.handleClick}>
                        <div className={[menuStyles.menuButtonBar, "menuBar"].join(' ')}></div>
                        <div className={[menuStyles.menuButtonBar, "menuBar"].join(' ')}></div>
                        <div className={[menuStyles.menuButtonBar, "menuBar"].join(' ')}></div>
                    </div>
                    <div className={[menuStyles.menuOptionsList, "menuItensBlock"].join(' ')}>
                        <div className={"menuItensBlockLists"}>
                            <Link to="/escola" className="menuItensBlockListItem">Escola</Link>
                            <Link to="/formacao" className="menuItensBlockListItem">Formação</Link>
                            <Link to="/cursos" className="menuItensBlockListItem">Cursos</Link>
                            <Link to="/termos" className="menuItensBlockListItem">Termos de privacidade</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>  
        );
      }
}
export default MenuComponent;