import React from "react"
import $ from "jquery"

class messageTab extends React.Component {

    componentDidMount() {
        $( document ).ready(function(){
            let messageTab = $('.messageTab');
            let closeMessage = $('.messageClose');

            closeMessage.click(function(){
                $(this).parent(messageTab).fadeOut();
            })
        });
    }

    render(){
        const iconType = this.props.iconType;
        const iconClassName = this.props.iconClassName;
        const messageTitle = this.props.messageTitle;
        const titleClassName = this.props.titleClassName;
        const messageText = this.props.messageText;
        const textClassName = this.props.textClassName;
        return(
            <div>
                <div className="messageTab bgPurple roundBorderDefault fontWhite">
                    <div className="messageIcon displayFlex">
                        <div className={["iconYem", iconClassName].join(' ')}></div>
                    </div>
                    <div className="messageContent">
                        <p className={["textBold", titleClassName].join(' ')}>{messageTitle}</p>
                        <p className={[textClassName].join(' ')}>{messageText}</p>
                    </div>
                    <div className="messageClose">
                        <div className="iconYem cancel"></div>
                    </div>
                </div>
            </div>
        )
    }
}
export default messageTab