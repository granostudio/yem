import React from "react"
import Container from "../../components/container/container"
import twoCircleBlockStyles from './twoCircleBlock.module.scss'
import threeCircleBlockStyles from "../threeCircleBlock/threeCircleBlock.module.scss"

import ImgYoga1 from '../../images/icon.png';
import ImgYoga2 from '../../images/icon2.png';

class twoCircleBlock extends React.Component {
    render() {
        return( 
            <div>
                <div className={[threeCircleBlockStyles.topBlock, twoCircleBlockStyles.customTwoDiv1].join(' ')} style={{backgroundImage: ["url(", this.props.topBgImage, ")"].join('') }}>
                    <Container>
                        <div className={[threeCircleBlockStyles.topBlockContent, twoCircleBlockStyles.customTwoPratique].join(' ')}>
                            <h2 className={["fontLightGrey",threeCircleBlockStyles.topBlockTitle].join(' ')}>{this.props.topTitle}</h2>
                            <p className={["fontLightGrey"].join(' ')}>{this.props.topText1}</p>
                            <p className={["fontLightGrey"].join(' ')}>{this.props.topText2}</p>
                            <p className={["fontLightGrey"].join(' ')}>{this.props.topText3}</p>
                            <p className={["fontLightGrey"].join(' ')}>{this.props.topText4}</p>
                            <p className={["fontLightGrey"].join(' ')}>{this.props.topText5}</p>
                        </div>
                    </Container>
                </div>
                <div className={[threeCircleBlockStyles.infoBlock, "lightGreyBg"].join(' ')}>
                    <Container>
                        <div className={[threeCircleBlockStyles.infoBlockList, "displayFlex", twoCircleBlockStyles.customTwoBlocks].join(' ')}>
                            <div className={[threeCircleBlockStyles.infoListItem, "smBlock lightGreyBg roundBorderDefault", twoCircleBlockStyles.customTwoBlock].join(' ')}>
                                <div className={[threeCircleBlockStyles.infoItemIcon, "displayFlex flexFullCenter lightGreyBg roundBorderFull"].join(' ')}>
                                    {/* <div className={["iconYem clockSimple iconXXl fontDarkGrey", threeCircleBlockStyles.icon].join(' ')}></div> */}
                                    <img src={ImgYoga1} className={twoCircleBlockStyles.customTwoIcon} />
                                </div>
                                <div className={[threeCircleBlockStyles.infoItemContent, "displayFlex flexColumn flexFullCenter"].join(' ')}>
                                    <h3 className={["fontBlack alignCenter", threeCircleBlockStyles.itemContentTitle].join(' ')}>Iniciantes Fácil</h3>
                                    <p className={["alignCenter", threeCircleBlockStyles.itemContentText].join(' ')}>voltado para quem está sedentário e tem dificuldades com exercícios</p>
                                </div>
                            </div>

                            <div className={[threeCircleBlockStyles.infoListItem, "smBlock lightGreyBg roundBorderDefault", twoCircleBlockStyles.customTwoBlock].join(' ')}>
                                <div className={[threeCircleBlockStyles.infoItemIcon, "displayFlex flexFullCenter lightGreyBg roundBorderFull"].join(' ')}>
                                    {/* <div className={["iconYem heart iconXXl fontDarkGrey", threeCircleBlockStyles.icon].join(' ')}></div> */}
                                    <img src={ImgYoga2} className={twoCircleBlockStyles.customTwoIcon} />
                                </div>
                                <div className={[threeCircleBlockStyles.infoItemContent, "displayFlex flexColumn flexFullCenter"].join(' ')}>
                                    <h3 className={["fontBlack alignCenter", threeCircleBlockStyles.itemContentTitle].join(' ')}>Iniciantes</h3>
                                    <p className={["alignCenter", threeCircleBlockStyles.itemContentText].join(' ')}>voltado para quem gosta de trabalhar o corpo</p>
                                </div>
                            </div>
                        </div>
                    </Container>
                </div>
            </div>
        )
    }
}
export default twoCircleBlock;