import React from "react";
import Slider from "react-slick";
import carouselDefaultStyles from "../carouselBlock/carouselDefault.scss"
import carouselCourseStyles from "../carouselBlock/carouselCourse/carouselCourse.module.scss"
import carouselCourse from "../carouselBlock/carouselCourse/carouselCourse.scss"
import Container from "../container/container"
import CourseSliderStyles from "./styles/coursesSlider.module.scss"



import coursePhoto from "../../images/courses-slider-bg.png"
import instructorPhoto from "../../images/instructor-small-img.png"

function CarouselNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div className={carouselCourseStyles.arrowBlock}>
      <div className={carouselCourseStyles.arrowBlockText}  onClick={onClick}>
        Ver outros Cursos
      </div>
      <div
        className={["iconYem arrowRight fontLightGrey iconXXl",className, carouselCourseStyles.arrowItem ].join(' ')}
        style={{ ...style}}
        onClick={onClick}
      />
    </div>
  );
}

function CarouselPrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={["iconYem arrowLeft fontLightGrey",className].join(' ')}
      style={{ ...style }}
      onClick={onClick}
    />
  );
}
 
class coursesSlider extends React.Component {
  render() {
    var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      nextArrow: <CarouselNextArrow />,
      prevArrow: <CarouselPrevArrow />,
      slidesToShow: this.props.numberSlidesView,
      appendDots: dots => <ul>{dots}</ul>,
      dotsClass: [carouselCourseStyles.dotsBlock, "dotsBlock"].join(' '),
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToScroll: 1,
            infinite: true,
          }
        },
        {
          breakpoint: 680,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };

    const slides = [
      {
        id: 1,
        courseBg: coursePhoto,
        courseName: 'NOME DO CURSO',
        courseDescription: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ',
        instructorImage: instructorPhoto,
        instructorName: 'Nome do professor',
        intructorLink: "#",
        courseHours: 4,
        discount: true,
        courseOriginalPrice: "100,00",
        discountPercentage: "50",
        coursePrice: "49,00",
        paymentInstallments: "3x"
      },
      {
        id: 2,
        courseBg: coursePhoto,
        courseName: 'NOME DO CURSO',
        courseDescription: 'Descubra a importância dos conhecimentos de Anatomia do Corpo Humano para os Praticantes e Professores de Yoga',
        instructorImage: instructorPhoto,
        instructorName: 'Nome do professor',
        intructorLink: "#",
        courseHours: 4,
        discount: true,
        courseOriginalPrice: "100,00",
        discountPercentage: "50",
        coursePrice: "49,00",
        paymentInstallments: "3"
      },
      {
        id: 3,
        courseBg: coursePhoto,
        courseName: 'NOME DO CURSO',
        courseDescription: 'Descubra a importância dos conhecimentos de Anatomia do Corpo Humano para os Praticantes e Professores de Yoga',
        instructorImage: instructorPhoto,
        instructorName: 'Nome do professor',
        intructorLink: "#",
        courseHours: 4,
        discount: true,
        courseOriginalPrice: "100,00",
        discountPercentage: "50",
        coursePrice: "49,00",
        paymentInstallments: "3"
      }
    ]

    return (
    <div>
      <Slider className={["carouselBlock", carouselCourseStyles.carouselCourse ].join(' ')} {...settings}>
        {/* ITENS SLIDER */}
        {slides.map(function(i){
          return (
            <div key={ i.id } id={i.id} className={[carouselCourseStyles.courseItem, CourseSliderStyles.courseItem ].join(' ')}>
              <div className={[carouselCourseStyles.courseItemContent,  CourseSliderStyles.courseItemContent].join(' ')} style={{backgroundImage: ["url(", i.courseBg, ")"].join(' ')}}>
                <Container internClass={carouselCourseStyles.courseItemContainer}>
                    <h2 className={["fontLightGrey", carouselCourseStyles.courseItemTitle].join(' ')}>{i.courseName}</h2>
                    <div className={[CourseSliderStyles.courseInfoBlock, "displayFlex"].join(' ')}>
                      <div className={CourseSliderStyles.infoImgBlock}>
                        <div className={CourseSliderStyles.courseImageCircle} style={{backgroundImage: ["url(", i.instructorImage ,")"].join(' ')}}></div>
                      </div>
                      <div className={[CourseSliderStyles.infoTextBlock, "flexFullCenterSelf"].join(' ')}>
                        <p className="fontLightGrey">{i.instructorName}</p>
                        <a href={i.intructorLink} className={"defaultLink"}>conheça o professor</a>
                      </div>
                    </div>
                    <p className={["fontLightGrey", CourseSliderStyles.descriptionText].join(' ')}>{i.courseDescription}</p>
                    <div className={[carouselCourseStyles.discountItemBlock, CourseSliderStyles.discountItemBlock].join(' ')}>
                        <div className={carouselCourseStyles.oldPriceBlock}>
                            <p className={["fontLightGrey", carouselCourseStyles.oldPriceText].join(' ')}>de R${i.courseOriginalPrice}</p>
                            <div className={["fontLightGrey roundBorderSmall", carouselCourseStyles.discountPercentBlock].join(' ')}>{i.discountPercentage}% off</div>
                        </div>
                        <p className={"textBlack fontLightGrey"}>por R$ {i.coursePrice} </p>
                        <p className={"fontLightGrey"}>em até {i.paymentInstallments} sem júros</p>
                    </div>
                    <div className={carouselCourseStyles.registrationBlock}>
                        <button className={["buttonMd alignCenter", carouselCourseStyles.registrationButton].join(' ')}>matricular</button>
                        <p className={["fontLightGrey textBlack", carouselCourseStyles.registrationText].join(' ')}>vagas limitadas!</p>
                    </div>
                </Container>
              </div>
            </div>
          ) 
        })}
        {/* FIM ITENS SLIDER */}
      </Slider>
    </div>
    );
  }
}
export default coursesSlider