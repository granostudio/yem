import React from "react"
import Select, {components} from 'react-select'
import selectFieldStyles from "./selectField.module.scss"

class selectField extends React.Component {
    
    render(){
        const options = this.props.options;
        const placeholder = this.props.placeholder;

        const Placeholder = props => {
            return <components.Placeholder {...props} />;
        };

        const customStyles = {
            container: (provided, state) => ({
                ...provided,
                outline: state.isFocused ? 'none' : 'none',
                // minWidth: '150px',
                maxWidth: '250px',
                minWidth: state.isFocused ? '150px' : 'unset',
            }),
            control: (provided, state) => ({
                ...provided,
                backgroundColor: '#252525',
                border: '1px solid transparent',
                outline: state.isFocused ? 'none' : 'none',
                // boxShadow: state.isSelected ? ' ' : '0 0 0 1px #8d36a6',
                boxShadow: state.isFocused ? '0 0 0 1px #8d36a6' : ' ',
                '&:hover': { borderColor: '#8d36a6', cursor: "pointer", color: '#8d36a6'  }, // border style on hover
                }),
            indicatorSeparator: (provided, state) => ({
                ...provided,
                display: 'none'
            }),
            dropdownIndicator: (provided, state) => ({
                ...provided,
                paddingLeft: '0'
            }),
            option: (provided, state) => ({
                backgroundColor: state.isSelected ? '#8d36a6' : '#252525',
                padding: '2px 8px',
                color: '#f5f5f5',
                letterSpacing: '0.13em',
                fontSize: '0.8888rem',
                '&:hover': { cursor: "pointer", color: '#8d36a6'  },
            }),
            singleValue: (provided, state) => ({
                color: state.isFocused ? '#f5f5f5' : '#8d36a6',
            }),
            menu: (provided, state) => ({
                
            }),
            placeholder: (provided, state) => ({
                position: 'relative',
                top: 'unset',
                transform: 'none',
                color: '#f5f5f5',
                display: state.isFocused ? 'none' : 'block',
                letterSpacing: '0.13em',
                fontSize: '0.8888rem',
            }),
            menuList: (provided, state) => ({
                backgroundColor: '#1d1c1c',
                border: '1px solid #8d36a6',
                marginTop: '0px',
                position: 'absolute',
                width: '100%',
                zIndex: '5',
                maxHeight: '150px',
                overflowY: 'scroll'
            }),
            input: (provided, state) => ({
                color: '#f5f5f5',
                letterSpacing: '0.13em',
                fontSize: '0.8888rem',
            })
        }
        return(
            <div className={selectFieldStyles.selectField}>
                <Select 
                styles={customStyles}
                options={options} 
                components={{ Placeholder }}
                placeholder={placeholder}
                />
            </div>
        )
    }
}
export default selectField