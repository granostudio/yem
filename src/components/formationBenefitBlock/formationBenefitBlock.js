import React from "react"
import threeBenefitsBlockStyles from "../threeBenefitsBlock/threeBenefitsBlock.module.scss"
import formationBenefitsStyles from "./formation.module.scss"
import Container from "../container/container"

import CountUp from 'react-countup';

export default props => (
    <div className={[threeBenefitsBlockStyles.threeBenefits, props.className].join(' ')}>
        <Container>
            {/* BOTAO PARA ROLAGEM DA PAGINA */}
            <a href={props.linkScroll}>
                <div className={["iconYem arrowDown", threeBenefitsBlockStyles.linkBtn].join(' ')}></div>
            </a>
            {/* FIM DO BOTAO PARA ROLAGEM DA PAGINA */}
            <div className={threeBenefitsBlockStyles.threeBenefitsBlock}>
                <div className={[threeBenefitsBlockStyles.BenefitsBlockItem,"smBlock"].join(' ')}>
                    <div className={["iconYem iconLg fontLightGrey",props.firstBlockIcon, threeBenefitsBlockStyles.benefitIcon].join(' ')}></div>
                    <h2 className={["fontLightGrey"]}>
                        <CountUp  
                        prefix={props.firstBlockPrefix}
                        suffix={[props.firstBlockSuffix].join(' ')}
                        duration={6}
                        start={0}
                        end={props.countOfClasses} />
                    </h2>
                    <p className={["fontLightGrey",threeBenefitsBlockStyles.BenefitsBlockText].join(' ')}>
                        {props.firstBlockText}
                    </p>
                </div>
                <div className={[threeBenefitsBlockStyles.BenefitsBlockItem,"smBlock"].join(' ')}>
                    <div className={["iconYem iconLg fontLightGrey", props.secondBlockIcon,threeBenefitsBlockStyles.benefitIcon].join(' ')}></div>
                    <h2 className={["fontLightGrey",].join(' ')}>
                        <CountUp  
                        prefix={props.secondBlockPrefix}
                        suffix={[props.secondBlockSuffix].join(' ')}
                        duration={6}
                        start={0}
                        end={props.secondCountOfClasses} />
                    </h2>
                    <p className={["fontLightGrey", threeBenefitsBlockStyles.BenefitsBlockText].join(' ')}>
                        {props.secondBlockText}
                    </p>
                </div>
                <div className={[threeBenefitsBlockStyles.BenefitsBlockItem,"smBlock"].join(' ')}>
                    <div className={["iconYem iconLg fontLightGrey", props.thirdBlockIcon,threeBenefitsBlockStyles.benefitIcon].join(' ')}></div>
                    <h2 className={["fontLightGrey alignCenter",formationBenefitsStyles.formationTitle].join(' ')}>
                        {props.thirdBlockTitle}
                    </h2>
                    <p className={["fontLightGrey"]}>{props.thirdBlockValueText}</p>
                    <button className={["buttonMd", formationBenefitsStyles.thirdBlockButton].join(' ')}>{props.thirdBlockButtonText}</button>
                    <p className={"fontLightGrey textBold titleSizeSm"}>{props.thirdBlockBoldText}</p>
                </div>
            </div>
        </Container>
    </div>

)