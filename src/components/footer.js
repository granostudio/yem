import React from "react"
import { Link } from "gatsby"
import Container from "./container/container"
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faFacebookF, faYoutube, faInstagram } from '@fortawesome/free-brands-svg-icons'
import footerStyles from "./footer.module.scss"
export default () => (
    <div className={footerStyles.footer}>
        <Container>
            <div className={footerStyles.footerMainContent}>
                <div className={footerStyles.footerMainContentMenu}>
                    <h4 className={["textBlack", footerStyles.footerMainTitle].join(' ')}>Perguntas frequêntes</h4>
                    <Link to="/">Formação profissional</Link>
                    <Link to="/">Área do aluno</Link>
                    <Link to="/">Cadastrar</Link>
                    <Link to="/">Contato</Link>
                    <div className={"socialBlockList"}>
                        <a href="" className={footerStyles.footerSocialIcon}>
                            <FontAwesomeIcon icon={faFacebookF}  />
                        </a>
                        <a href="" className={footerStyles.footerSocialIcon}>
                            <FontAwesomeIcon icon={faYoutube}  />
                        </a>
                        <a href="" className={footerStyles.footerSocialIcon}>
                            <FontAwesomeIcon icon={faInstagram}  />
                        </a>
                    </div>
                </div>
                <div className={footerStyles.footerNews}>
                    <div className={["displayFlex flexFullCenter bgLightGrey",footerStyles.footerNewsImageBlock].join(' ')}>
                        <div className={["fontPurple",footerStyles.whatsIcon].join(' ')}></div>
                    </div>
                    <div className={footerStyles.footerNewsContentBlock}>
                        <p className={["textBold",footerStyles.footerNewsBoldText].join(' ')}>Entre em contato pelo Whatsapp</p>
                        <p className={footerStyles.footerNewsSmallText}>(11) XXXX-XXXX</p>
                    </div>
                </div>
            </div>
            <div className={footerStyles.copyrightBlock}>
                <div className={footerStyles.copyrightBlockText}>
                    <p><span className={["iconYem lock", footerStyles.lock].join(' ')}></span> Site completamente seguro com SSL &bull; Copyright &copy; 2019</p>
                    <p>Yoga em Movimento - CNPJ 12.772.498/0001-90 - Rua Analder Santana, 230, Vinhedo, SP, Brasil - CEP 13282-538</p>
                </div>
                <div className={["menuLogo",footerStyles.copyrightBlockLogo].join(' ')}></div>
            </div>
        </Container>
    </div>
)