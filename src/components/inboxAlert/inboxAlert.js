import React from "react"
import inboxAlertStyles from "./inboxAlert.module.scss"

class inboxAlert extends React.Component {
    render() {
        const inboxAlertCount = this.props.inboxAlertCount
        return(
            <div className={inboxAlertStyles.inboxContent}>
                <div className={inboxAlertStyles.inboxBlock}>
                    <div className={[inboxAlertStyles.iconYem, "iconYem inbox fontLightGrey"].join(' ')}></div>
                    <div className={["fontLightGrey", inboxAlertStyles.inboxCount, this.props.inboxAlertCount ? '' : inboxAlertStyles.inboxCountHidden].join(' ')}>{inboxAlertCount}</div>
                </div>
            </div>
        )
    }
}
export default inboxAlert