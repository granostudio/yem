import React, { Component } from "react"
import { Link } from "gatsby"
import classNames from 'classnames';
import menuStyles from "./menu.module.scss"


class MenuComponent extends Component { 
    state = {
        isActive: false
    };

    handleClick = () => {
        this.setState(state => ({ isActive: !state.isActive }));
    };

    render() {
        const menuClass = classNames({
          menu: true,
          menuActive: this.state.isActive
        });
    
        return (
        <div className={menuStyles.menu}>
            <Link to="/">
                <div className={menuStyles.menuLogo}></div>
            </Link>
            <div className={menuStyles.menuBtnBlock}>
                <div className={menuStyles.menuLinksBlock}>
                    <Link to="/" className={[menuStyles.menuLinksItem, menuStyles.menuLinksItemHalfActive].join(' ')}>entrar</Link>
                    <Link to="/" className={[menuStyles.menuLinksItem, menuStyles.menuLinksItemActive].join(' ')}>cadastrar</Link>
                    <Link to="/" className={[menuStyles.menuLinksItem, menuStyles.menuLinksItemHalfActive].join(' ')} onClick={this.handleClick}>menu</Link>
                </div>
                <div className={[menuStyles.menuButton, menuClass].join(' ')} onClick={this.handleClick}>
                    <div className={[menuStyles.menuButtonBar, "menuBar"].join(' ')}></div>
                    <div className={[menuStyles.menuButtonBar, "menuBar"].join(' ')}></div>
                    <div className={[menuStyles.menuButtonBar, "menuBar"].join(' ')}></div>
                </div>
                <div className={[menuStyles.menuOptionsList, "menuItensBlock"].join(' ')}>
                    <div className={"menuItensBlockLists"}>
                        <Link to="/escola" className="menuItensBlockListItem">Escola</Link>
                        <Link to="/formacao" className="menuItensBlockListItem">Formação</Link>
                        <Link to="/cursos" className="menuItensBlockListItem">Cursos</Link>
                        <Link to="/termos" className="menuItensBlockListItem">Termos de privacidade</Link>
                    </div>
                </div>
            </div>
        </div>  
        );
      }
}
export default MenuComponent;