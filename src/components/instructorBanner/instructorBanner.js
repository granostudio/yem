import React from "react"
import { Link } from "gatsby"
import instructorBannerStyles from "./instructorBanner.module.scss"

class instructorBanner extends React.Component {
    render() {
        const bannerTopTitle = this.props.bannerTopTitle;
        const bannerImg = this.props.bannerImg;
        const bannerTitle = this.props.bannerTitle;
        const monthClass = this.props.monthClass;
        const yearClass = this.props.yearClass;
        const numberOfInstallments = this.props.numberOfInstallments;
        const priceOfInstallments = this.props.priceOfInstallments;
        return(
            <div className={instructorBannerStyles.banner}>
                <h3 className={["fontLightGrey", instructorBannerStyles.bannerTopTitle].join(" ")}>{bannerTopTitle}</h3>
                <div className={instructorBannerStyles.bannerContent} style={{backgroundImage: ["url(",bannerImg,")"].join('')}}>
                    <div className={instructorBannerStyles.contentBlock}>
                        <h2 className={[instructorBannerStyles.contentTitle, "fontLightGrey"].join(" ")}>{bannerTitle}</h2>
                        <div className={instructorBannerStyles.contentInfoBlock}>
                            <div className={["iconYem calendar fontLightGrey", instructorBannerStyles.infoIcon].join(" ")}></div>
                            <div className={[instructorBannerStyles.contentInfoTextBlock].join(" ")}>
                                <p className={["fontLightGrey", instructorBannerStyles.contentInfoTopText].join(" ")}>próxima turma</p>
                                <div className={[instructorBannerStyles.infoTextGroup].join(" ")}>
                                    <p className={"fontLightGrey"}>
                                        <span className={[instructorBannerStyles.blockInfoText].join(" ")}>{monthClass}</span>
                                        <span className={[instructorBannerStyles.blockInfoText].join(" ")}>{yearClass}</span>
                                    </p>
                                    <p className={"fontLightGrey"}>
                                        <span className={[instructorBannerStyles.blockInfoText].join(" ")}>{[numberOfInstallments, "x"].join("")}</span>
                                        <span className={[instructorBannerStyles.blockInfoText].join(" ")}>{["R$", priceOfInstallments].join("")}</span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <Link to="#" className="buttonMd">matricular</Link>
                    </div>
                </div>
            </div>
        )
    }
}
export default instructorBanner