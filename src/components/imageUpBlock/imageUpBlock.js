import React from "react"
import imageUpBlockStyles from "./imageUpBlock.module.scss"
import Container from "../container/container"
import PracticeBlock from "../practiceBlock/practiceBlock"

import YogaDaniel from "../../images/yoga-daniel-tonet.png"
import YogaGirlOutside from "../../images/yoga-woman-up-block.png"

export default props => (
    <div className={["darkGreyBg", imageUpBlockStyles.imageUpBlock].join(' ')}>
        <Container>
            <div className={imageUpBlockStyles.imageUpBlockContainer}>
                <div className={["mdBlock",imageUpBlockStyles.imageUpBlockItem].join(' ')}>
                    <div className={[imageUpBlockStyles.imageUpItem].join(' ')} style={{background: ["url(",YogaGirlOutside,")no-repeat"].join(' ')}}>

                    </div>
                </div>
                <div className={["mdBlock", imageUpBlockStyles.imageUpTextBlock].join(' ')}>
                    <h2 className={[imageUpBlockStyles.imageUpTitle, "fontLightGrey"].join(' ')}>Sempre com você</h2>
                    
                    <div className={imageUpBlockStyles.benefitsBlockList}>
                        <div className={imageUpBlockStyles.benefitsBlockListItem}>
                            <div className={["iconYem userIcon fontLightGrey", imageUpBlockStyles.benefitsItem].join(' ')}></div>
                            <p className="fontLightGrey">Acesse a sua área do aluno</p>
                        </div>
                        <div className={imageUpBlockStyles.benefitsBlockListItem}>
                            <div className={["iconYem thumbsUp fontLightGrey", imageUpBlockStyles.benefitsItem].join(' ')}></div>
                            <p className="fontLightGrey">Escolha a sua aula</p>
                        </div>
                        <div className={imageUpBlockStyles.benefitsBlockListItem}>
                            <div className={["iconYem iconWink fontLightGrey", imageUpBlockStyles.benefitsItem].join(' ')}></div>
                            <p className={"fontLightGrey"}>Aperte o play e boa aula!</p>
                        </div>
                        <PracticeBlock 
                            darkBg={true} 
                            titleText={"Pratique em todos os momentos"}
                            practiceText={"Assista na sua TV, celular ou tablet, de forma simples e fácil"}
                        />
                    </div>
                </div>
            </div>
        </Container>
    </div>
)