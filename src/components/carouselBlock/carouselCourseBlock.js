import React from "react";
import Slider from "react-slick";
import carouselDefaultStyles from "./carouselDefault.scss"
import carouselCourseStyles from "./carouselCourse/carouselCourse.module.scss"
import carouselCourse from "./carouselCourse/carouselCourse.scss"
import Container from "../container/container"
import ThreeBenefitsBlock from "../threeBenefitsBlock/threeBenefitsBlock"

import coursePhoto from "../../images/mulher-fazendo-yoga.png"
import instructorPhoto from "../../images/carla_darcanchy.png"

function CarouselNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div className={carouselCourseStyles.arrowBlock}>
      <div className={carouselCourseStyles.arrowBlockText}  onClick={onClick}>
        Ver outros Cursos
      </div>
      <div
        className={["iconYem arrowRight fontLightGrey iconXXl",className, carouselCourseStyles.arrowItem ].join(' ')}
        style={{ ...style}}
        onClick={onClick}
      />
    </div>
  );
}

function CarouselPrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={["iconYem arrowLeft fontLightGrey",className].join(' ')}
      style={{ ...style }}
      onClick={onClick}
    />
  );
}
 
class carouselCourseBlock extends React.Component {
  render() {
    var settings = {
      dots: true,
      infinite: true,
      speed: 500,
      nextArrow: <CarouselNextArrow />,
      prevArrow: <CarouselPrevArrow />,
      slidesToShow: this.props.numberSlidesView,
      appendDots: dots => <ul>{dots}</ul>,
      dotsClass: [carouselCourseStyles.dotsBlock, "dotsBlock"].join(' '),
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToScroll: 1,
            infinite: true,
          }
        },
        {
          breakpoint: 680,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };

    const slides = [
      {
        id: 1,
        courseBg: coursePhoto,
        courseName: 'NOME DO CURSO',
        courseDescription: 'Descubra a importância dos conhecimentos de Anatomia do Corpo Humano para os Praticantes e Professores de Yoga',
        instructorImage: instructorPhoto,
        instructorName: 'professor',
        courseHours: 4,
        discount: true,
        courseOriginalPrice: "100,00",
        discountPercentage: "50",
        coursePrice: "49,00",
        paymentInstallments: "3x"
      },
      {
        id: 2,
        courseBg: coursePhoto,
        courseName: 'NOME DO CURSO',
        courseDescription: 'Descubra a importância dos conhecimentos de Anatomia do Corpo Humano para os Praticantes e Professores de Yoga',
        instructorImage: instructorPhoto,
        instructorName: 'professor',
        courseHours: 4,
        discount: true,
        courseOriginalPrice: "100,00",
        discountPercentage: "50",
        coursePrice: "49,00",
        paymentInstallments: "3"
      },
      {
        id: 3,
        courseBg: coursePhoto,
        courseName: 'NOME DO CURSO',
        courseDescription: 'Descubra a importância dos conhecimentos de Anatomia do Corpo Humano para os Praticantes e Professores de Yoga',
        instructorImage: instructorPhoto,
        instructorName: 'professor',
        courseHours: 4,
        discount: true,
        courseOriginalPrice: "100,00",
        discountPercentage: "50",
        coursePrice: "49,00",
        paymentInstallments: "3"
      }
    ]

    return (
    <div>
      <Slider className={["carouselBlock", carouselCourseStyles.carouselCourse ].join(' ')} {...settings}>
        {/* ITENS SLIDER */}
        {slides.map(function(i){
          return (
            <div key={ i.id } id={i.id} className={carouselCourseStyles.courseItem}>
              <div className={carouselCourseStyles.courseItemContent} style={{backgroundImage: ["url(", i.courseBg, ")"].join(' ')}}>
                <Container internClass={carouselCourseStyles.courseItemContainer}>
                    <h2 className={["fontLightGrey", carouselCourseStyles.courseItemTitle].join(' ')}>{i.courseName}</h2>
                    <p className={["fontLightGrey", carouselCourseStyles.courseItemText].join(' ')}>{i.courseDescription}</p>
                    <div className={carouselCourseStyles.discountItemBlock}>
                        <div className={carouselCourseStyles.oldPriceBlock}>
                            <p className={["fontLightGrey", carouselCourseStyles.oldPriceText].join(' ')}>de R${i.courseOriginalPrice}</p>
                            <div className={["fontLightGrey roundBorderSmall", carouselCourseStyles.discountPercentBlock].join(' ')}>{i.discountPercentage}% off</div>
                        </div>
                        <p className={"textBlack fontLightGrey"}>por R$ {i.coursePrice} </p>
                        <p className={"fontLightGrey"}>em até {i.paymentInstallments} sem júros</p>
                    </div>
                    <div className={carouselCourseStyles.registrationBlock}>
                        <button className={["buttonMd alignCenter", carouselCourseStyles.registrationButton].join(' ')}>matricular</button>
                        <p className={["fontLightGrey textBlack", carouselCourseStyles.registrationText].join(' ')}>vagas limitadas!</p>
                    </div>
                    <ThreeBenefitsBlock
                      linkScroll="#nextBlock"
                      firstBlockIcon="clockSimple"
                      countOfClasses={4}
                      firstBlockSuffix="Horas"
                      firstBlockText="aulas em vídeo"
                      // secondBlockIcon="heart"
                      benefitImgTest={instructorPhoto}
                      secondBlockTitle="professor"
                      secondBlockText="conheça o professor"
                      secondTextClass="fontPurple"
                      thirdBlockTitle="comece hoje grátis "
                      thirdBlockButtonText="cadastrar"
                    >

                    </ThreeBenefitsBlock>
                </Container>
              </div>
              
            </div>
            
          ) 
        })}
        {/* FIM ITENS SLIDER */}
      </Slider>
    </div>
    );
  }
}
export default carouselCourseBlock