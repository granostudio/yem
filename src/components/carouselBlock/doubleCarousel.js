import React, { Component } from "react";
import Slider from "react-slick";

import DoubleCarouselDefaultStyles from "./doubleCarousel/doubleCarousel.scss"
import DoubleCarouselStyles from "./doubleCarousel/doubleCarousel.module.scss"

import MaterialImg from "../../images/bookSliderImg.png"

export default class AsNavFor extends Component {
  constructor(props) {
    super(props);
    this.state = {
      nav1: null,
      nav2: null
    };
  }

  componentDidMount() {
    this.setState({
      nav1: this.slider1,
      nav2: this.slider2
    });
  }

  render() {

    const slides = [
      {
        photo: MaterialImg,
        name: 'E-book Ásana',
        text: 'Lorem ipsum dolor sit amet, consectetur elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation',
        id: 1
      },
      {
        photo: MaterialImg,
        name: 'E-book Integer ',
        text: 'Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia Curae; Duis vulputate tortor non tellus auctor mollis.',
        id: 2
      },
      {
        photo: MaterialImg,
        name: 'E-book Pellentesque',
        text: 'Integer eleifend faucibus dolor sit amet laoreet. Nulla dictum quis nibh sed condimentum. Morbi suscipit at diam non consectetur. Aliquam rutrum ut lectus sed tempus.',
        id: 3
      }
    ]

    return (
      <div className={"doubleCarrousel displayFlex flexWrap"}>
          <div className={["mdBlock", DoubleCarouselStyles.leftCarouselBlock].join(' ')}>
            <div className={[DoubleCarouselStyles.topBlock].join(' ')}>
              <h3 className={"titleSizeLg textBlack"}>{this.props.leftBlockTitle}</h3>
              <p>{this.props.leftBlockText}</p>
            </div>
            <Slider
            asNavFor={this.state.nav2}
            ref={slider => (this.slider1 = slider)}
            arrows={false}
            fade={true}
            >
            {slides.map(function(i){
              return (
                <div key={ i.id } >
                  <p className={["fontDarkGrey textBlack"].join(' ')}>{i.name}</p>
                  <p className={["fontDarkGrey"].join(' ')}>{i.text}</p>
                </div>
                
              ) 
            })}
            </Slider>
          </div>
          <div className={"mdBlock rightCarouselBlock"}>
                <Slider
                asNavFor={this.state.nav1}
                ref={slider => (this.slider2 = slider)}
                slidesToShow={1}
                swipeToSlide={true}
                focusOnSelect={true}
                fade={true}
                prevArrow={<div> <div className={["iconYem arrowLeft fontDarkGrey", DoubleCarouselStyles.arrow, DoubleCarouselStyles.arrowLeft].join(' ')}></div> </div>}
                nextArrow={<div> <div className={["iconYem arrowRight fontDarkGrey", DoubleCarouselStyles.arrow, DoubleCarouselStyles.arrowRight].join(' ')}></div> </div>}
                >
                {slides.map(function(i){
                  return (
                    <div key={ i.id } >
                      <div>
                        <img src={i.photo}></img>
                      </div>
                    </div>
                    
                  ) 
                })}
                </Slider>
          </div>
      </div>
    );
  }
}