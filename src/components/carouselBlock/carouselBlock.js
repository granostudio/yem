import React from "react";
import Slider from "react-slick";
import carouselDefaultStyles from "./carouselDefault.scss"
import carouselInstructorStyles from "./carouselInstructor.module.scss"

import carouselImg from "../../images/carousel-img.png"

function CarouselNextArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={["iconYem arrowRight fontLightGrey",className].join(' ')}
      style={{ ...style}}
      onClick={onClick}
    />
  );
}

function CarouselPrevArrow(props) {
  const { className, style, onClick } = props;
  return (
    <div
      className={["iconYem arrowLeft fontLightGrey",className].join(' ')}
      style={{ ...style }}
      onClick={onClick}
    />
  );
}
 
class SimpleSlider extends React.Component {
  render() {
    var settings = {
      dots: false,
      infinite: true,
      speed: 500,
      nextArrow: <CarouselNextArrow />,
      prevArrow: <CarouselPrevArrow />,
      slidesToShow: this.props.numberSlidesView,
      slidesToScroll: 1,
      responsive: [
        {
          breakpoint: 1024,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            infinite: true,
            dots: false
          }
        },
        {
          breakpoint: 680,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
          }
        },
        {
          breakpoint: 480,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1
          }
        }
      ]
    };

    const slides = [
      {
        photo: carouselImg,
        name: 'Profº Daniel Tonet',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor',
        id: 1
      },
      {
        photo: carouselImg,
        name: 'Profº Daniel Tonet',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor',
        id: 2
      },
      {
        photo: carouselImg,
        name: 'Profº Daniel Tonet',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor',
        id: 3
      },
      {
        photo: carouselImg,
        name: 'Profº Daniel Tonet',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor',
        id: 4
      },
      {
        photo: carouselImg,
        name: 'Profº Daniel Tonet',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor',
        id: 5
      },
      {
        photo: carouselImg,
        name: 'Profº Daniel Tonet',
        text: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor',
        id: 6
      },
    ]

    return (
    <div>
      <div className={[carouselInstructorStyles.sliderTopContainer,"alignCenter"].join(' ')}>
        <h2 className={"fontDarkGrey"}>{this.props.introTitle}</h2>
        <p className={"fontDarkGrey"}>{this.props.firstParagraph}</p>
        <p className={"fontDarkGrey"}>{this.props.secondParagraph}</p>
      </div>
      <Slider className={"carouselBlock"} {...settings}>
        {/* ITENS SLIDER */}
        {slides.map(function(i){
          return (
            <div key={ i.id } className={carouselInstructorStyles.carouselInstructorItem}>
              <div className={carouselInstructorStyles.instructorItemImg}>
                <img src={i.photo}></img>
              </div>
              <p className={["fontDarkGrey", carouselInstructorStyles.instructorItemName].join(' ')}>{i.name}</p>
              <p className={["fontDarkGrey", carouselInstructorStyles.instructorItemText].join(' ')}>{i.text}</p>
            </div>
            
          ) 
        })}
        {/* FIM ITENS SLIDER */}
      </Slider>
      {/* <div className={carouselInstructorStyles.sliderBottomContainer}>
        <div className={["mdBlock",carouselInstructorStyles.mediumBlock].join(' ')}>
          <p className={"textBold alignRight fontLightGrey"}>Ensine e compartilhe as riquezas do Yôga</p>
          <p className={"alignRight fontLightGrey"}>A nossa formação de instrutores contempla um</p>
          <p className={"alignRight fontLightGrey"}>curso com mais de 200 horas e certificado</p>
        </div>
        <div className={["mdBlock displayFlex",carouselInstructorStyles.mediumBlock].join(' ')}>
          <button className={["buttonMd", carouselInstructorStyles.sliderBottomBtn].join(' ')}>quero conhecer</button>
        </div>
      </div> */}
    </div>
    );
  }
}
export default SimpleSlider