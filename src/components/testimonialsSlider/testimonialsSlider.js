import React from "react"
import Container from "../container/container"
import testimonialsStyles from "../testimonials/testimonials.module.scss"
import testimonialsSliderStyles from "./testimonialsSlider.module.scss"

import Slider from "react-slick";

class testimonialsSlider extends React.Component {
    render() {
        const settings = {
            dots: true,
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            vertical: true,
            verticalSwiping: true,
            beforeChange: function(currentSlide, nextSlide) {
              console.log("before change", currentSlide, nextSlide);
            },
            afterChange: function(currentSlide) {
              console.log("after change", currentSlide);
            }
        };
        return(
            <div className="darkGreyBg">
                <Container>
                    <div className={testimonialsStyles.testimonialBlock}>
                        <div className={["smBlock",testimonialsStyles.testimonialSmBlock].join(' ')}>
                            <div className={testimonialsStyles.infoBlock}>
                                <h2 className={[testimonialsStyles.infoTitle, 'fontLightGrey'].join(' ')}>O que os nossos alunos dizem</h2>
                                <div className={[testimonialsStyles.startTodayBlock].join(' ')}>
                                    <p className={[testimonialsStyles.infoText, 'fontLightGrey'].join(' ')}>Comece hoje <span className="textBold">grátis</span></p>
                                    <button className={["buttonMd",testimonialsStyles.infoBtn ].join(' ')}>cadastrar</button>
                                    <p className={['fontLightGrey', testimonialsStyles.infoSmallText].join(' ')}>cancele quando quiser</p>
                                </div>
                            </div>
                        </div>
                        <div className={["lgBlock", testimonialsStyles.testimonialLgBlock].join(' ')}>
                            <Slider {...settings}>
                                <div>
                                    <div>
                                        <div>
                                            <div></div>
                                            <div>
                                                <p>O Yôga em movimento é 10! O istrutores são ótimos! Obrigado por toda atenção!</p>
                                                <p>Fernanda Lima</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div>
                                            <div></div>
                                            <div>
                                                <p>O Yôga em movimento é 10! O istrutores são ótimos! Obrigado por toda atenção!</p>
                                                <p>Fernanda Lima</p>
                                            </div>  
                                        </div>
                                        <div>
                                            <div></div>
                                            <div>
                                                <p>O Yôga em movimento é 10! O istrutores são ótimos! Obrigado por toda atenção!</p>
                                                <p>Fernanda Lima</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </Slider>
                        </div>
                    </div>
                </Container>
            </div>
        )
    }
}
export default testimonialsSlider