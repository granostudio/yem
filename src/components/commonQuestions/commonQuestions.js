import React from "react"
import commonQuestionsStyles from "./commonQuestions.module.scss"
import $ from "jquery"

class commonQuestions extends React.Component {
    // constructor () {
    //     super()
    //     this.state = {
    //       isHidden: true
    //     }
    //   }
    //   toggleHidden () {
    //     this.setState({
    //       isHidden: !this.state.isHidden
    //     })
    //   }
    componentDidMount() {
        $('.listItem').click(function() {
          $(this).toggleClass(commonQuestionsStyles.listItemInactive);
        });
    }
    render(){
        return(
            <div className={commonQuestionsStyles.commonQuestionsBlock}>
                <h2 className={[commonQuestionsStyles.commonQuestionsTitle, "alignCenter fontLightGrey"].join(' ')}>Perguntas frequêntes</h2>
                <div className={[commonQuestionsStyles.listBlock, "roundBorderDefault lightGreyBg"].join(' ')}>
                    <div 
                        className={["listItem", commonQuestionsStyles.listBlockItem, commonQuestionsStyles.listItemInactive].join(' ')}
                    >
                        <h4 className={commonQuestionsStyles.listBlockItemTitle}>Pergunta 1</h4>
                        <div className={commonQuestionsStyles.itemTextBlock}>
                            <p>cdantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                        </div>
                    </div>

                    <div 
                        className={["listItem", commonQuestionsStyles.listBlockItem, commonQuestionsStyles.listItemInactive].join(' ')}
                    >
                        <h4 className={commonQuestionsStyles.listBlockItemTitle}>Pergunta 1</h4>
                        <div className={commonQuestionsStyles.itemTextBlock}>
                            <p>cdantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
export default commonQuestions