import React from "react"
import threeBenefitsBlockStyles from "./threeBenefitsBlock.module.scss"
import Container from "../container/container"

export default props => (
    <div className={threeBenefitsBlockStyles.threeBenefits}>
        <Container>
            {/* BOTAO PARA ROLAGEM DA PAGINA */}
            <a href={props.linkScroll}>
                <div className={["iconYem arrowDown", threeBenefitsBlockStyles.linkBtn].join(' ')}></div>
            </a>
            {/* FIM DO BOTAO PARA ROLAGEM DA PAGINA */}
            <div className={threeBenefitsBlockStyles.threeBenefitsBlock}>
                <div className={[threeBenefitsBlockStyles.BenefitsBlockItem,"smBlock"].join(' ')}>
                    <div className={["iconYem iconLg fontLightGrey",props.firstBlockIcon, threeBenefitsBlockStyles.benefitIcon].join(' ')}></div>
                    <h2 className={["fontLightGrey"]}>
                        {props.firstBlockTitle}
                    </h2>
                    <p className={["fontLightGrey",threeBenefitsBlockStyles.BenefitsBlockText].join(' ')}>
                        {props.firstBlockText}
                    </p>
                </div>
                <div className={[threeBenefitsBlockStyles.BenefitsBlockItem,"smBlock"].join(' ')}>
                    <div className={["iconYem iconLg fontLightGrey", props.secondBlockIcon,threeBenefitsBlockStyles.benefitIcon].join(' ')}></div>
                    <h2 className={["fontLightGrey",].join(' ')}>
                        {props.secondBlockTitle}
                    </h2>
                    <p className={["fontLightGrey",].join(' ')}>
                        {props.secondBlockText}
                    </p>
                </div>
                <div className={[threeBenefitsBlockStyles.BenefitsBlockItem,"smBlock"].join(' ')}>
                    <h2 className={["fontLightGrey",threeBenefitsBlockStyles.BenefitsBlockTitle].join(' ')}>
                        {props.thirdBlockTitle}
                    </h2>
                    <button className={"buttonMd"}>{props.thirdBlockButtonText}</button>
                    <p className={"fontLightGrey"}>{props.thirdBlockText}</p>
                </div>
            </div>
        </Container>
    </div>

)