import React from "react"
import threeBenefitsBlockStyles from "./threeBenefitsBlock.module.scss"
import Container from "../container/container"

import CountUp from 'react-countup';

export default props => (
    <div className={[threeBenefitsBlockStyles.threeBenefits, props.className].join(' ')}>
        <Container>
            {/* BOTAO PARA ROLAGEM DA PAGINA */}
            <a href={props.linkScroll}>
                <div className={["iconYem arrowDown", threeBenefitsBlockStyles.linkBtn].join(' ')}></div>
            </a>
            {/* FIM DO BOTAO PARA ROLAGEM DA PAGINA */}
            <div className={[threeBenefitsBlockStyles.threeBenefitsBlock].join(' ')}>
                <div className={[threeBenefitsBlockStyles.BenefitsBlockItem,"smBlock"].join(' ')}>
                    <div className={["iconYem iconLg fontLightGrey",props.firstBlockIcon, threeBenefitsBlockStyles.benefitIcon].join(' ')}></div>
                    <h2 className={["fontLightGrey"]}>
                        <CountUp  
                        prefix={[" ",props.firstBlockPrefix].join(' ')}
                        suffix={[" ",props.firstBlockSuffix].join(' ')}
                        duration={6}
                        start={0}
                        end={props.countOfClasses} />
                    </h2>
                    <p className={["fontLightGrey",threeBenefitsBlockStyles.BenefitsBlockText].join(' ')}>
                        {props.firstBlockText}
                    </p>
                </div>
                <div className={[threeBenefitsBlockStyles.BenefitsBlockItem,"smBlock"].join(' ')}>
                    <div className={["iconYem iconLg fontLightGrey", props.secondBlockIcon,threeBenefitsBlockStyles.benefitIcon, props.benefitImgTest ? threeBenefitsBlockStyles.benefitItemImg : ' ' ].join(' ')} style={{backgroundImage: ["url(", props.benefitImgTest ? props.benefitImgTest : ' ' , ")"].join(' ')}} ></div>
                    <h2 className={["fontLightGrey",].join(' ')}>
                        {props.secondBlockTitle}
                    </h2>
                    <p className={["fontLightGrey",props.secondTextClass].join(' ')}>
                        {props.secondBlockText}
                    </p>
                </div>
                <div className={[threeBenefitsBlockStyles.BenefitsBlockItem,"smBlock"].join(' ')}>
                    <h2 className={["fontLightGrey",threeBenefitsBlockStyles.BenefitsBlockTitle].join(' ')}>
                        {props.thirdBlockTitle}
                    </h2>
                    <button className={"buttonMd"}>{props.thirdBlockButtonText}</button>
                    <p className={"fontLightGrey"}>{props.thirdBlockText}</p>
                </div>
            </div>
        </Container>
    </div>

)