import React from "react"
import { Link } from "gatsby"
import Header from "../components/header"
import SocialBlock from "../components/socialListColumn/socialListColumn"
import IntroVideo from "../components/imageTextIntro/introWithVideo"
import FormationBenefitsBlock from "../components/formationBenefitBlock/formationBenefitBlock"
import Container from "../components/container/container"
import FormacaoStyles from "./page-styles/formacao.module.scss"
import CarouselBlock from "../components/carouselBlock/carouselBlock"
import CourseContent from "../components/courseContent/courseContent"
import MaterialCarousel from "../components/carouselBlock/doubleCarousel"
import TestimonialSimple from "../components/testimonials/testimonialSimpleBlock"
import CommonQuestions from "../components/commonQuestions/commonQuestions"
import Footer from "../components/footer"

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faWhatsapp } from '@fortawesome/free-brands-svg-icons'

import formacaoBg from "../images/formacaoBg.png"
import noteImg from "../images/formacao-note.png"
import cellphoneImg from "../images/formacao-tablet.png"
import whatsPhoneImg from "../images/formacao-smartphone.png"
import certifiedLogo from "../images/certified-logo.png"
import namasteIcon from "../images/namaste-icon.png"

export default () => (
    <div>
        <Header />
        <SocialBlock 
            facebookUrl="https://pt-br.facebook.com/" 
            youtubeUrl="https://www.youtube.com/?gl=BR&hl=pt"
            instragramUrl="https://www.instagram.com/?hl=pt-br"
        />
        <IntroVideo
            introImg={formacaoBg}
            contentBlockCustomClass={FormacaoStyles.introTextBlock}
            titleCustomClass={FormacaoStyles.introTitle}
            iconClass={"play"}
            introTitle={"seja instrutor de yoga"}
            firstTextCustomClass={FormacaoStyles.introText}
            firstIntroText={"o melhor e mais completo curso de formação e aperfeiçoamento de instrutores de Yoga está ao seu alcance."}
            secondTextCustomClass={FormacaoStyles.introText}
            secondIntroText={"E com a comodidade do ensino a distância."}
        >
            <FormationBenefitsBlock 
                linkScroll="#imgUpBlock"
                firstBlockIcon="clockSimple"
                countOfClasses={200}
                firstBlockSuffix=" horas"
                firstBlockText="de coteúdo e 10 meses de duração,
                seguindo o padrão internacional de formação de instrutores.   "
                secondBlockIcon="desktop"
                secondCountOfClasses={100}
                secondBlockSuffix="% online"
                secondBlockText="centenas de aulas práticas, teóricas e treinamento de meditação, mantras, respiratórios e muito mais."
                thirdBlockIcon="calendar"
                thirdBlockTitle="próxima turma"
                thirdBlockValueText="JAN 2020 - 10x R$365"
                thirdBlockButtonText="cadastrar"
                thirdBlockBoldText="vagas limitadas!"
            />
        </IntroVideo>
        <div className={"bgLightGrey"}>
            <Container>
                <div className={["fullBlock", FormacaoStyles.techniquesTopBlock].join(' ')}>
                    <h2 className={["alignCenter titleSizeLg", FormacaoStyles.techniquesTopTitle].join(' ')}>Técnica, Filosofia, Prática, Didática e Profissão</h2>
                    <p className={["alignCenter", FormacaoStyles.techniquesTopText].join(' ')}>O riquíssimo coteúdo do curso abrange todo o conteúdo necessário a um excelente de Yoga. As aulas são disponibilizadas semanalmente para você assistir quando a quantas vezes quiser. Além das aulas teóricas, vocie terá acesso integral e vitalício a todo o conteúdo de aulas práticas disponível na nossa escola.</p>
                </div>
            </Container>
            <Container>
                <div className={FormacaoStyles.techniquesInfoBlock}>
                    <div className={["displayFlex",FormacaoStyles.liveClasses, FormacaoStyles.techniquesItem].join(' ')}>
                        <div className={["mdBlock", FormacaoStyles.liveClassesImgBlock].join(' ')}>
                            <div className={[FormacaoStyles.liveClassesImgItem].join(' ')} style={{backgroundImage: ["url(", noteImg,")"].join(' ')}}></div>
                        </div>
                        <div className={["mdBlock", FormacaoStyles.techniquesItemText].join(' ')}>
                            <h3 className={["textBlack titleSizeLg", FormacaoStyles.itemTitle].join(' ')}>30 aulas ao vivo</h3>
                            <p className={[FormacaoStyles.itemText, FormacaoStyles.liveClassesText].join(' ')}>Encontros quinzenais por videoconferência para tiriar todas as sua dúvidas e ter certeza de que o conteúdo e as técnicas estão sendo assimilados perfeitamente.</p>
                            <Link to="/" className="buttonMd">programação</Link>
                        </div>
                    </div>
                </div>
            </Container>
            <div className={FormacaoStyles.techniquesInfoBlock}>
                <div className={["displayFlex",FormacaoStyles.liveLectures, FormacaoStyles.techniquesItem].join(' ')}>
                    <div className={["mdBlock", FormacaoStyles.techniquesItemText].join(' ')}>
                        <div className={[FormacaoStyles.liveLecturesTextContent, "displayFlex flexColumn"].join(' ')}>
                            <h3 className={["textBlack alignRight titleSizeLg", FormacaoStyles.itemTitle].join(' ')}>10 palestras ao vivo</h3>
                            <p className={[ "alignRight",FormacaoStyles.itemText].join(' ')}>Alguns dos mais renomados professores de Yoga da atualidade enriquecerão o seu aprendizado, ampliando seus horizontes. Você conhecerá o Yoga sob vários pontos de vista diferentes e terá a oportunidade de desenvolver uma visão mais ampla e tolerante do universo do Yoga.</p>
                            <Link to="/" className="buttonMd">programação</Link>
                        </div>
                    </div>
                    <div className={[FormacaoStyles.liveLecturesImgBlock].join(' ')}>
                        <div className={[FormacaoStyles.liveLecturesImgItem].join(' ')} style={{backgroundImage: ["url(", cellphoneImg ,")"].join(' ')}}></div>
                    </div>
                </div>

                <div className={["displayFlex",FormacaoStyles.closeTrack, FormacaoStyles.techniquesItem].join(' ')}>
                    <div className={["mdBlock",FormacaoStyles.closeTrackImgBlock].join(' ')}>
                        <div className={[FormacaoStyles.closeTrackImgItem].join(' ')} style={{backgroundImage: ["url(", whatsPhoneImg ,")"].join(' ')}}></div>
                    </div>
                    <div className={["mdBlock", FormacaoStyles.techniquesItemText].join(' ')}>
                        <div className={[FormacaoStyles.closeTrackTextContent, "displayFlex flexColumn"].join(' ')}>
                            <h3 className={["textBlack alignLeft titleSizeLg", FormacaoStyles.itemTitle].join(' ')}>Acompanhamento próximo</h3>
                            <p className={[ "alignLeft",FormacaoStyles.itemText].join(' ')}>Durante o curso - e até mesmo depois dele - você será acompanhado de perto por um dos nossos professores.</p>
                            <p className={[ "alignLeft",FormacaoStyles.itemText].join(' ')}>Os grupos exclusivos no WhatsApp e Facebook são outros canais de comunicação com a nossa equipe de experiências entre os colegas.</p>
                            <Link to="/" className="buttonMd">programação</Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div className={["bgDarkGrey", FormacaoStyles.iYC].join(' ')}>
            <Container internClass={FormacaoStyles.certifiedContainer}>
                <div className={FormacaoStyles.certifiedBlock}>
                    <h3 className={["fontLightGrey alignCenter titleSizeLg"].join(' ')}>Curso com certificado</h3>
                    <h3 className={["fontLightGrey alignCenter titleSizeLg"].join(' ')}>e chancelado pelo International Yoga Council</h3>
                    <p className={["fontLightGrey alignCenter", FormacaoStyles.certifiedText].join(' ')}>Ao término do curso, ao ser aprovado nas avaliações teóricas e práticas, você receberá o Certificado Internacional de Instrutor de Yoga e poderá ministrar suas aulas em estúdios, academias, espaços aberto, colégios, clínicas... onde quiser! Poderá, inclusive, abrir seu próprio espaço de Yoga.</p>
                </div>
                <div className={["displayFlex",FormacaoStyles.certifiedListBlock].join(' ')}>
                    <div className={[FormacaoStyles.certifiedListItem, "smBlock"].join(' ')}>
                        <div className={["iconYem iconXXl globe fontLightGrey", FormacaoStyles.certifiedItemImgBlock].join(' ')}></div>
                        <h3 className={["fontLightGrey alignCenter titleSizeLg"].join(' ')}>validade internacional</h3>
                        <p className={["fontLightGrey alignCenter", FormacaoStyles.listItemText].join(' ')}>com o certificado você pode ministrar aulas em qualquer lugar do Brasil e até mesmo no expterior</p>
                    </div>
                    <div className={[FormacaoStyles.certifiedListItem, "smBlock"].join(' ')}>
                        <div className={[FormacaoStyles.certifiedItemImgBlock].join(' ')}>
                            <div className={FormacaoStyles.certifiedItemLogo} style={{backgroundImage: ["url(", namasteIcon,")"].join(' ')}}></div>
                        </div>
                        <h3 className={["fontLightGrey alignCenter titleSizeLg"].join(' ')}>International Yoga Council</h3>
                        <p className={["fontLightGrey alignCenter", FormacaoStyles.listItemText].join(' ')}> um reconhecimento da qualidade e seriedade do curso </p>
                    </div>
                    <div className={[FormacaoStyles.certifiedListItem, "smBlock"].join(' ')}>
                        <div className={["iconYem iconXXl gradCap fontLightGrey", FormacaoStyles.certifiedItemImgBlock].join(' ')}></div>
                        <h3 className={["fontLightGrey alignCenter titleSizeLg"].join(' ')}>emissão e envio de certificado</h3>
                        <p className={["fontLightGrey alignCenter", FormacaoStyles.listItemText].join(' ')}>você receberá impresso em papel de alta qualidade na sua casa, sem custo adicional.</p>
                    </div>
                </div>
            </Container>
        </div>
        <div className={"bgLightGrey"} style={{paddingTop: "3.888rem"}}>
            <Container>
                <CarouselBlock 
                introTitle={"Seus professores"}
                firstParagraph={"a nossa equipe conta com décadas de experiência"}
                secondParagraph={"e todos estão sempre a disposição para acompanhar o seu desenvolvimento"}
                numberSlidesView={4} />
                <CarouselBlock 
                firstParagraph={"O curso contará também com a participação especial de vários professores convidados que enriquecerão o aprendizado e ampliar os horizontes dos praticantes"}
                numberSlidesView={4} />
            </Container>
            <Container internClass={FormacaoStyles.supportMaterial}>
                <MaterialCarousel
                    leftBlockTitle={"Material de apoio"}
                    leftBlockText={"você receberá um vasto material de apoio que facilitará os estudos, que se tornará uma excelente fonte de consultas durante toda a sua carreira de instrutor de Yoga."}
                    numberSlidesView={1}
                />
            </Container>
        </div>
        <div className={"bgDarkGrey"}>
            <Container>
                <CourseContent />
            </Container>
        </div>
        <div className={"bgLightGrey"}>
            <Container internClass={["displayFlex", FormacaoStyles.testimonialBlockContainer].join(' ')}>
                <div className={["smBlock", FormacaoStyles.testimonialBlock].join(' ')}>
                    <div className={FormacaoStyles.testimonialInfoBlock}>
                        <h3 className="titleSizeLg">O que os nossos alunos dizem</h3>
                        <div className={FormacaoStyles.calendarInfoBlock}>
                            <div className={"iconYem calendar iconXXl"}></div>
                            <div className={FormacaoStyles.iconTextBlock}>
                                <p className="textBlack">próxima turma</p>
                                <p className="titleSizeSm">JAN 2020 - 10x R$365</p>
                            </div>
                        </div>
                        <div className={[FormacaoStyles.limitedButtonBlock].join(' ')}>
                            <Link className="buttonMd">matricular</Link>
                            <p className={["textBlack", FormacaoStyles.limitedBlackText].join(' ')}>vagas limitadas!</p>
                        </div>
                    </div>
                </div>
                <div className={["lgBlock", FormacaoStyles.testimonialList].join(' ')}>
                    <TestimonialSimple></TestimonialSimple>
                </div>
            </Container>
        </div>
        <div className={"bgDarkGrey"}>
            <Container>
                <div className={[FormacaoStyles.whatsBlock, "alignCenter"].join(' ')}>
                    <FontAwesomeIcon icon={faWhatsapp} className={["fontLightGrey titleSizeXl", FormacaoStyles.whatsIcon].join(' ')} />
                    <p className="textBlack fontLightGrey titleSizeSm">Quer conversar com um dos nossos ministrantes?</p>
                    <p className="fontLightGrey titleSizeSm">WhatsApp: <a href="tel:6198115-4849" className={["fontLightGrey", FormacaoStyles.whatsLink].join(' ')}>61  98115-4849</a></p>
                </div>
                <CommonQuestions />
            </Container>
        </div>
        <Footer />
    </div>
)