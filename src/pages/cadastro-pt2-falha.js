import React from "react"
import ReactDOM from 'react-dom';
import $ from 'jquery'
import Header from "../components/headerSimple/headerSimple"
import Container from "../components/container/container"
import Footer from "../components/footer"

import checkoutStyles from "./page-styles/checkout.module.scss"


class CadastroPt2Falha extends React.Component {

    render() {
        return(
            <div>
                <Header/>
                    <div>
                        <Container className={checkoutStyles.containerPt2}>
                            <div className={checkoutStyles.checkoutBlock}>
                              <p className="alignCenter">Passo 2 de 2</p>
                              <h2 className="alignCenter">Cartão não aprovado</h2>
                            </div>
                            <div>
                              <div className={["displayFlex flexFullCenter", checkoutStyles.checkoutBlock].join(' ')}>
                                  <div className={["iconYem iconSad", checkoutStyles.sadIcon].join(' ')}></div>
                              </div>
                            </div>
                            <div className={["flexFullCenter2 flexColumn", checkoutStyles.checkoutBlock].join(' ')}>
                              <p>A operadora do seu cartão nos informou o seguinte:</p>
                              <p>"Mensagem da operadora”</p>
                            </div>
                            <div className={["flexFullCenter2", checkoutStyles.checkoutBlock].join(' ')}>
                              <button className={["buttonMd"].join(' ')}>tentar novamente</button>
                            </div>
                        </Container>
                    </div>
                <Footer />
            </div>
        )
    }
}

export default CadastroPt2Falha