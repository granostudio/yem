import React from "react"
import GradientBlock from "../components/gradientBlock/gradientBlock"
import Container from "../components/container/container"
import Header from "../components/header"
import Footer from "../components/footer"

export default () => (
  <GradientBlock>
    <Header></Header>
    <div className="lightGreyBg">
      <Container>
        <div></div>
        <h1>Titulo H1</h1>
        <h2>Titulo H2</h2>
        <div style={{ width: "400px", margin: "1.944rem 0 0 " }}>
          <p style={{ marginBottom: "1.111rem" }}>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim.
          </p>
          <p>
            Ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute
            irure dolor in reprehen
          </p>
        </div>
        <div style={{ display: "flex", width: "50%", marginTop: "2.5rem" }}>
          <div style={{ width: "50%", display: "flex", alignItems: "center" }}>
            <button className={"buttonMd"}>cadastrar</button>
          </div>
          <div style={{ width: "150px", display: "flex", flexWrap: "wrap" }}>
            <div
              className={"iconYem iconLg gradCap"}
              style={{ width: "50%" }}
            ></div>
            <div
              className={"iconYem iconLg diamond"}
              style={{ width: "50%" }}
            ></div>
            <div
              className={"iconYem iconLg thumbsUp"}
              style={{ width: "50%" }}
            ></div>
            <div
              className={"iconYem iconLg heart"}
              style={{ width: "50%" }}
            ></div>
            <div
              className={"iconYem iconLg iconWink"}
              style={{ width: "50%" }}
            ></div>
            <div
              className={"iconYem iconLg iconSad"}
              style={{ width: "50%" }}
            ></div>
            <div
              className={"iconYem iconLg iconHappy"}
              style={{ width: "50%" }}
            ></div>
            <div
              className={"iconYem iconLg lock"}
              style={{ width: "50%" }}
            ></div>
          </div>
        </div>
      </Container>
      <Container>
        <div className="messageTab bgPurple roundBorderDefault fontWhite">
          <div className="messageIcon displayFlex">
            <div className="iconYem iconHappy"></div>
          </div>
          <div className="messageContent">
            <p className="textBold">Atenção</p>
            <p>Você ainda precisa confirmar o seu email, enviamos um link para vi******@gmail.****</p>
          </div>
          <div className="messageClose">
            <div className="iconYem cancel"></div>
          </div>
        </div>
      </Container>
    </div>
    <Footer />
  </GradientBlock>
)
