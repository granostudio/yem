import React from "react"
import { Link } from "gatsby"
import Header from "../components/header"
import Container from "../components/container/container"
import SliderTop from "../components/coursesSlider/coursesSlider"
import SocialList from "../components/socialListColumn/socialListColumn"
import RollDownButton from "../components/rollDownButton/rollDownButton"
import CourseList from "../components/courseList/courseList"
import ImageUpBlock from "../components/imageUpBlock/imageUpBlock"
import CommonQuestions from "../components/commonQuestions/commonQuestions"

import Footer from "../components/footer"

import CoursesStyles from "../pages/page-styles/cursos.module.scss"

export default () => (
    <div>
        <Header />
        <SliderTop 
            numberSlidesView={1}
        />
        <SocialList 
            facebookUrl="https://pt-br.facebook.com/" 
            youtubeUrl="https://www.youtube.com/?gl=BR&hl=pt"
            instragramUrl="https://www.instagram.com/?hl=pt-br"
        />
        <RollDownButton linkRollDown="#maximize"  />
        <div className="bgLightGrey" id="maximize">
            <Container>
                <CourseList
                    blockTitle={"Mais conhecimento para maximizar a sua evolução no Yoga" }
                    titleclassName={CoursesStyles.courseTitle}
                />
                <div className={["displayFlex flexFullCenter", CoursesStyles.courseListButtonBlock].join(' ')}>
                    <Link className="buttonMd" to="todos os cursos">todos os cursos</Link>
                </div>
            </Container>
        </div>
        <div className="bgDarkGrey">
            <ImageUpBlock/>
            <Container>
                <CommonQuestions/>
            </Container>
        </div>
        <Footer />
    </div>
)