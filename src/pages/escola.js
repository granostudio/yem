import React from "react"
import Header from "../components/header"
import Container from "../components/container/container"
import TextCircleImgBlock from "../components/textCircleImgBlock/textCircleImgBlock"
import SocialBlock from "../components/socialListColumn/socialListColumn"
import IntroVideo from "../components/imageTextIntro/introWithVideo"
import ThreeBenefitsBlock from "../components/threeBenefitsBlock/threeBenefitsBlockDoubleCount"
import ThreeCircleBlock from "../components/threeCircleBlock/threeCircleBlock"
import PracticeBlock from "../components/practiceBlock/practiceBlock"
import StartTodayBlock from "../components/startTodayButtonBlock/startTodayButtonBlock"
import Testimonial from "../components/testimonials/testimonials"
import AdvantagesListBlock from "../components/advantagesListBlock/advantagesListBlock" 
import CarouselBlock from "../components/carouselBlock/carouselBlock"
import CourseList from "../components/courseList/courseList"
import CommonQuestions from "../components/commonQuestions/commonQuestions"

import EscolaStyles from "./page-styles/escola.module.scss"

import EscolaBg from "../images/escolaBg.png"
import BenefitImg from "../images/benefitBlockImg.png"
import YogaBg from "../images/circleYogaBg.png"
import AdvImg1 from "../images/advantage-img1.png"
import AdvImg2 from "../images/advantage-img2.png"
import AdvImg3 from "../images/advantage-img3.png"

import Footer from "../components/footer"

export default () => (
    <div>
        <Header/>
        <SocialBlock 
            facebookUrl="https://pt-br.facebook.com/" 
            youtubeUrl="https://www.youtube.com/?gl=BR&hl=pt"
            instragramUrl="https://www.instagram.com/?hl=pt-br"
        />
        <IntroVideo
            introImg={EscolaBg}
            contentBlockCustomClass={EscolaStyles.introBlock}
            titleCustomClass={EscolaStyles.escolaTitle}
            iconClass={"play"}
            introTitle={"A sua escola de Yôga Online"}
            // firstTextCustomClass={}
            firstIntroText={"o mundo do Yoga ao seu alcance"}
            // secondTextCustomClass={}
            // secondIntroText={"E com a comodidade do ensino a distância."}
        >
            <ThreeBenefitsBlock 
                linkScroll="#ondeVoceEstiver"
                firstBlockIcon="usersIcon"
                firstBlockPrefix="+"
                countOfClasses={800}
                firstBlockSuffix=" alunos"
                firstBlockText="junte-se ao enorme grupo de alunos que praticam Yoga conosco "
                secondBlockIcon="videoCam"
                secondBlockPrefix="+"
                secondCountOfClasses={350}
                secondBlockSuffix=" aulas"
                secondBlockText="centenas de aulas práticas, teóricas e treinamento de meditação, mantras, respiratórios e muito mais."
                thirdBlockTitle="comece hoje grátis "
                thirdBlockButtonText="cadastrar"
                thirdBlockText="cancele quando quiser"
                className={EscolaStyles.threeBenefitsBlock}
            />
        </IntroVideo>
        <div id="ondeVoceEstiver">
            <TextCircleImgBlock
                benefitImg={BenefitImg}
            />
        </div>
        <ThreeCircleBlock 
        topBgImage={YogaBg}
        topTitle={"Pratique do seu jeito"}
        topText1={"Aulas recomendadas com base no seu perfil, "}
        topText2={"ou siga os programas específicos para dores nas costas,"}
        topText3={"fortalecimento muscular, ganho de flexibilidade, entre outros,"}
        topText4={"ou selecione a aula ideia para o seu momento com filtros de:"}
        />
        <div className="lightGreyBg">
            <Container>
                <div className={["displayFlex flexFullCenter", EscolaStyles.practiceBlock].join(' ')}>
                    <div className="lgBlock displayFlex flexEnd">
                        <PracticeBlock 
                            darkBg={false} 
                            titleText={"Sempre com você"}
                            practiceText={"Assista na sua TV, celular ou tablet, de forma simples e fácil"}
                        />
                    </div>
                    <div className="smBlock">
                        <StartTodayBlock></StartTodayBlock>
                    </div>
                </div>
            </Container>
        </div>
        <div className={["bgDarkGrey", EscolaStyles.testimonialBlock ].join(' ')}>
            <Testimonial></Testimonial>
        </div>
        <div className="bgDarkGrey">
            <Container>
                <AdvantagesListBlock
                   firstBg={AdvImg1}
                   secondBg={AdvImg2}
                   thirdBg={AdvImg3}
                />
                <div className="displayFlex flexFullCenter" style={{padding: '2.2222rem 0'}}>
                    <button className="buttonMd buttonDefaultPadding">Tudo isso com acompanhamento bem próximo da nossa equipe de instrutores</button>
                </div>
            </Container>
        </div>
        <div className="bgLightGrey">
            <Container>
                <CarouselBlock numberSlidesView={4} />
            </Container>
            <Container internClass={EscolaStyles.courseListBlock}>
                <CourseList 
                    blockTitle={"Confira como são as aulas"}
                />
            </Container>
        </div>
        <div className="bgDarkGrey normalPaddingTop">
            <Container>
                <CommonQuestions/>
            </Container>
        </div>

        <Footer/>
    </div>
)