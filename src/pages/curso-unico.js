import React from "react"
import Header from "../components/header"
import CarouselCourse from "../components/carouselBlock/carouselCourseBlock"
import SocialBlock from "../components/socialListColumn/socialListColumn"
import Container from "../components/container/container"
import CourseList from "../components/courseContent/courseContent"
import ImageUpBlock from "../components/imageUpBlock/imageUpBlock"
import CommonQuestions from "../components/commonQuestions/commonQuestions"
import Footer from "../components/footer"

import WomanWithHat from "../images/woman-with-hat.png"
import CarlaDarcanchy from "../images/carla_darcanchy.png"

import cursoUnicoStyles from "./page-styles/curso-unico.module.scss"

class cursoUnicoPage extends React.Component {
    render() {
        return(
            <div>
                <Header />
                <CarouselCourse numberSlidesView={1} />
                <SocialBlock 
                    facebookUrl="https://pt-br.facebook.com/" 
                    youtubeUrl="https://www.youtube.com/?gl=BR&hl=pt"
                    instragramUrl="https://www.instagram.com/?hl=pt-br"
                />
                <div className="lightGreyBg" id="nextBlock">
                    <Container>
                        <div className={cursoUnicoStyles.aboutBlock}>
                            <h2 className={[cursoUnicoStyles.aboutTitle, "alignCenter"].join(' ')}>Sobre o Curso</h2>
                            <p className={[cursoUnicoStyles.aboutText, "alignCenter"].join(' ')}>O riquíssimo coteúdo do curso abrange todo o conteúdo necessário a um excelente de Yoga. As aulas são disponibilizadas semanalmente para você assistir quando a quantas vezes quiser. Além das aulas teóricas, vocie terá acesso integral e vitalício a todo o conteúdo de aulas práticas disponível na nossa escola.</p>
                        </div>
                    </Container>
                </div>
                <div className="bgDarkGrey">
                    <Container>
                        <CourseList />
                    </Container>
                </div>
                <div className={["bgLightGrey", cursoUnicoStyles.userBlock].join(' ')}>
                    <Container internClass={["displayFlex flexWrap", cursoUnicoStyles.userBlockItem].join(' ')}>
                        <div className={["mdBlock"].join(' ')}>
                            <div className={["roundBorderFull", cursoUnicoStyles.userImg].join(' ')} style={{backgroundImage: ["url(", WomanWithHat,")"].join(' ')}}></div>
                        </div>
                        <div className={"mdBlock displayFlex flexColumn flexFullCenter"}>
                            <h4 className={[cursoUnicoStyles.title, cursoUnicoStyles.userTitle].join(' ')}>Para quem é</h4>
                            <p className={cursoUnicoStyles.userText}>O riquíssimo coteúdo do curso abrange todo o conteúdo necessário a um excelente de Yoga. As aulas são disponibilizadas semanalmente para você assistir quando a quantas vezes quiser. Além das aulas teóricas, vocie terá acesso integral e vitalício a todo o conteúdo de aulas práticas disponível na nossa escola.</p>
                        </div>
                    </Container>
                    <Container internClass={["displayFlex flexWrap"].join(' ')}>
                        <div className={"mdBlock displayFlex flexColumn flexFullCenter"}>
                            <h4 className={[cursoUnicoStyles.title, cursoUnicoStyles.userTitle, "alignRight"].join(' ')}>Carla d'Arcanchy</h4>
                            <p className={["alignRight"].join(' ')}>Carla d´Arcanchy é professora desde 1997. Ministra aulas de Yoga, Pilates e Educação Física. Carioca de nascimento, vive em Brasília há mais de 30 anos. E foi exatamente na capital do país que Carla travou seu primeiro contato com a filosofia do Yoga. Praticou várias linhas, em muitas escolas e com diversos instrutores, até que se formou pela Primeira Universidade de Yoga do Brasil.</p>
                        </div>
                        <div className={["mdBlock"].join(' ')}>
                            <div className={[cursoUnicoStyles.userImg].join(' ')} style={{backgroundImage: ["url(", CarlaDarcanchy,")"].join(' ')}}></div>
                        </div>
                    </Container>
                </div>
                <div className="bgDarkGrey">
                    <Container>
                        <ImageUpBlock></ImageUpBlock>
                    </Container>
                    <Container>
                        <CommonQuestions />
                    </Container>
                </div>
                <Footer />
            </div>
        )
    }
}

export default cursoUnicoPage