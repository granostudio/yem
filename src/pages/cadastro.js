import React from "react"
import Header from "../components/headerSimple/headerSimple"
import Container from "../components/container/container"
import { Link } from "gatsby"
import SwitchInput from "../components/switchInput/switchInput"
import Footer from "../components/footer"

import checkoutStyles from "./page-styles/checkout.module.scss"

class Checkout extends React.Component {
    render() {
        return (
            <div>
                <Header/>
                    <div>
                        <Container internClass={checkoutStyles.checkoutContainer}>
                            <div style={{marginBottom: "2rem"}}>
                                <p className="alignCenter">Passo 1 de 2</p>
                                <h2 className="alignCenter">Cadastrar</h2>
                            </div>
                            <div>
                                <form className={"displayFlex flexAlignCenter flexColumn"}>
                                    <input type="text" className={"inputYem inputMd"} placeholder={"Como você prefere ser tratado?"} />
                                    <input type="tel" className={"inputYem inputMd"} placeholder={"Telefone"} />
                                    <div style={{marginBottom: '1.222rem'}}>
                                        <SwitchInput
                                            switchText={"este telefone possui whatsapp?"}
                                            textContainerClass={"alignRight"}
                                        />
                                    </div>
                                    <input type="text" className={"inputYem inputMd"} placeholder={"Nome Completo"} />
                                    <input type="email" className={"inputYem inputMd"} placeholder={"Email"} />
                                    <input type="password" className={"inputYem inputMd"} placeholder={"Senha"} />
                                    <input type="password" className={"inputYem inputMd"} placeholder={"Confirme sua senha"} />
                                    <Link to="/cadastro-pt2" className={["buttonMd", checkoutStyles.button].join(' ')}>continuar</Link>
                                </form>
                            </div>
                        </Container>
                    </div>
                <Footer />
            </div>
        )
    }
}
export default Checkout
