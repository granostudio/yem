import React from "react"
import ReactDOM from 'react-dom';
import $ from 'jquery'
import Header from "../components/headerSimple/headerSimple"
import Container from "../components/container/container"
import Cleave from 'cleave.js/react';
import ReactTooltip from 'react-tooltip'
import Footer from "../components/footer"

import checkoutStyles from "./page-styles/checkout.module.scss"


class CadastroPt2 extends React.Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            creditCardType:     '',
            creditCardRawValue: '',

            name: '',
            lastName: '',
            email: '',
            cvv: '',
            nameError: '',
            lastNameError: '',
            emailError: '',
            cvvError: ''
        };
        
        this.onCreditCardChange = this.onCreditCardChange.bind(this);
        this.onCreditCardTypeChanged = this.onCreditCardTypeChanged.bind(this);   

    }

    onCreditCardChange(event) {
        this.setState({creditCardRawValue: event.target.rawValue});
    }

    onCreditCardTypeChanged(type){
        this.setState({creditCardType: type});
    }

    onChange(event) {
        // formatted pretty value
        console.log(event.target.value);

        // raw value
        console.log(event.target.rawValue);
    }

    // VALIDACAO
      handleNameChange = event => {
        this.setState({ name: event.target.value }, () => {
          this.validateName();
        });
      };
      handleLastNameChange = event => {
        this.setState({ lastName: event.target.value }, () => {
          this.validateLastName();
        });
      };
    
      handleEmailChange = event => {
        this.setState({ email: event.target.value }, () => {
          this.validateEmail();
        });
      };

      handleCvvChange = event => {
        this.setState({ cvv: event.target.value }, () => {
          this.validateCvv();
        });
      };
    
      validateName = () => {
        const { name } = this.state;
        this.setState({
          nameError:
            name.length > 3 ? null : 'Nome não pode ter menos de 3 caracteres'
        });
      }

      validateLastName = () => {
        const { lastName } = this.state;
        this.setState({
          lastNameError:
            lastName.length > 3 ? null : 'Sobrenome não pode ter menos de 3 caracteres'
        });
      }
    
      validateEmail = () => {
        const { email } = this.state;
        this.setState({
          emailError:
            email.length > 3 ? null : 'Email não pode ter menos de 3 caracteres'
        });
      }

      validateCvv = () => {
        const { cvv } = this.state;
        this.setState({
          cvvError:
            cvv.length > 2 ? null : 'CVV não pode ter menos de 2 caracteres'
        });
      }
    
      handleSubmit = event => {
        event.preventDefault();
        const { name, email } = this.state;
        alert(`Your state values: \n 
                name: ${name} \n 
                email: ${email}`);
      };


    render() {
        const tooltipText = "O código de segurança de seu cartão (CVV) é o número de 3 ou 4 dígitos localizado no verso da maioria dos cartões"

        return(
            <div>
                <Header/>
                    <div>
                        <Container className={checkoutStyles.containerPt2}>
                            <div style={{marginBottom: "1.5555rem"}}>
                                <p className="alignCenter">Passo 2 de 2</p>
                                <h2 className="alignCenter">Pratique um mês grátis</h2>
                            </div>
                            <div>
                                <div className="displayFlex flexFullCenter">
                                    <div className="iconYem iconLg thumbsUp"></div>
                                    <p className="flexFullCenterSelf">A mensalidade é de R$ 47,00. Você só será cobrado quando seu mês grátis terminar.</p>
                                </div>
                                <div className="displayFlex flexFullCenter">
                                    <div className="iconYem iconLg thumbsUp"></div>
                                    <p className="flexFullCenterSelf">Sem compromisso, cancele quando quiser.</p>
                                </div>
                                <form className={"displayFlex flexAlignCenter flexColumn"}>
                                    <div id={"cardList"} className={checkoutStyles.cardList}>
                                        <div className={[checkoutStyles.cardItem, checkoutStyles.cardMaster, "cardItem mastercard"].join(' ')}></div>
                                        <div className={[checkoutStyles.cardItem, checkoutStyles.cardVisa, "cardItem visa"].join(' ')}></div>
                                        <div className={[checkoutStyles.cardItem, checkoutStyles.cardElo, "cardItem elo"].join(' ')}></div>
                                        <div className={[checkoutStyles.cardItem, checkoutStyles.cardDiners, "cardItem diners"].join(' ')}></div>
                                        <div className={[checkoutStyles.cardItem, checkoutStyles.cardAmerican, "cardItem american"].join(' ')}></div>
                                    </div>
                                    <input 
                                        name='name'
                                        type="text" 
                                        className={`inputYem inputMd ${this.state.nameError ? 'is-invalid' : ''}`} 
                                        placeholder={"Nome"} 
                                        value={this.state.name}
                                        onChange={this.handleNameChange}
                                        onBlur={this.validateName}
                                    />
                                    <div className='invalid-feedback'>{this.state.nameError}</div>
                                    <input 
                                        name='lastName'
                                        type="text" 
                                        className={`inputYem inputMd ${this.state.lastNameError ? 'is-invalid' : ''}`} 
                                        placeholder={"Sobrenome"} 
                                        value={this.state.lastName}
                                        onChange={this.handleLastNameChange}
                                        onBlur={this.validateLastName}
                                    />
                                    <div className='invalid-feedback'>{this.state.lastNameError}</div>
                                    <Cleave
                                        className={"inputYem inputMd"} 
                                        placeholder="Numero do Cartão"
                                        options={{creditCard: true, onCreditCardTypeChanged: function (type) {
                                            console.log(type);


                                            // CARTOES
                                            var cardItem = $('.cardItem');
                                            var masterCard = $(".mastercard");
                                            var visa = $(".visa");
                                            var elo = $(".elo");
                                            var diners = $(".diners");
                                            var american = $(".american");

                                            console.log(cardItem);
                                            if(type == 'unknown') {
                                                cardItem.removeClass(checkoutStyles.cardActive);
                                            } 
                                            if(type == 'mastercard') {
                                                cardItem.removeClass(checkoutStyles.cardActive);
                                                masterCard.addClass(checkoutStyles.cardActive); 
                                            }
                                            if(type == 'visa') {
                                                cardItem.removeClass(checkoutStyles.cardActive);
                                                visa.addClass(checkoutStyles.cardActive); 
                                            }
                                            if(type == 'diners') {
                                                cardItem.removeClass(checkoutStyles.cardActive);
                                                diners.addClass(checkoutStyles.cardActive); 
                                            }
                                            if(type == 'amex') {
                                                cardItem.removeClass(checkoutStyles.cardActive);
                                                american.addClass(checkoutStyles.cardActive); 
                                            }
                                        }
                                        }}
                                        onChange={this.onCreditCardChange}
                                    />     
                                    <Cleave
                                        className={"inputYem inputMd"} 
                                        placeholder="Data de Validade (MM/AA)"
                                        options={{date: true, datePattern: ['m', 'y']}}
                                    />                             
                                    <div className={["displayFlex flexFullCenter flexAlignCenter positionRelative inputYemBLock"].join(' ')} >
                                        <div>
                                            <input
                                                type={"number"}
                                                max={9999}
                                                className={`inputYem inputMd ${this.state.cvvError ? 'is-invalid' : ''}`} 
                                                placeholder="Codigo de Segurança (CVV)"
                                                value={this.state.cvv}
                                                onChange={this.handleCvvChange}
                                                onBlur={this.validateCvv}
                                            />  
                                            <div className='invalid-feedback'>{this.state.cvvError}</div>
                                        </div>
                                        <div className={checkoutStyles.tooltip}>
                                            <a data-tip={tooltipText} data-event='click focus' className="">?</a>
                                            <ReactTooltip className={"tooltipYem"} globalEventOff='click' place="right" type="dark" />
                                        </div>
                                    </div>
                                    <button className={["buttonMd", checkoutStyles.buttonPt2].join(' ')}>Iniciar assinatura</button>
                                </form>
                            </div>
                        </Container>
                    </div>
                <Footer />
            </div>
        )
    }
}

export default CadastroPt2