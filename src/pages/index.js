import React from "react"
import Header from "../components/header"
import Container from "../components/container/container"
import GradientBlock from "../components/gradientBlock/gradientBlock"
import SocialBlock from "../components/socialListColumn/socialListColumn"
import ImageTextIntro from "../components/imageTextIntro/imageTextIntro"
import ThreeBenefitsBlock from "../components/threeBenefitsBlock/threeBenefitsBlock"
import FourWindowsBlock from "../components/fourWindowsBlock/fourWindowsBlock"
import ImageUpBlock from "../components/imageUpBlock/imageUpBlock"
import CarouselBlock from "../components/carouselBlock/carouselBlock"
import TestimonialsSlider from "../components/testimonialsSlider/testimonialsSlider"
import Footer from "../components/footer"

import HomeStyles from "./page-styles/home.module.scss"

export default () => (
  <div className="bgDarkGrey">
    <Header />
    <ImageTextIntro>
      <ThreeBenefitsBlock 
        linkScroll="#imgUpBlock"
        firstBlockIcon="videoCam"
        firstBlockPrefix="+"
        countOfClasses={800}
        firstBlockText="aulas em vídeo"
        secondBlockIcon="heart"
        secondBlockTitle="professores"
        secondBlockText="sempre a disposição"
        thirdBlockTitle="comece hoje grátis "
        thirdBlockButtonText="cadastrar"
        thirdBlockText="cancele quando quiser"

        className={HomeStyles.introBlock}
        >
      </ThreeBenefitsBlock>
    </ImageTextIntro>
    <SocialBlock 
    facebookUrl="https://pt-br.facebook.com/" 
    youtubeUrl="https://www.youtube.com/?gl=BR&hl=pt"
    instragramUrl="https://www.instagram.com/?hl=pt-br"
    />
    <FourWindowsBlock />
    <div id="imgUpBlock">
      <ImageUpBlock />
    </div>
    {/* BLOCO SOBRE OS INSTRUTORES */}
  <div className={"bgLightGrey"} style={{paddingTop: "3.888rem"}}>
    <Container>
      <CarouselBlock 
        introTitle={"Seus professores"}
        firstParagraph={"a nossa equipe conta com décadas de experiência"}
        secondParagraph={"e todos estão sempre a disposição para acompanhar o seu desenvolvimento"}
        numberSlidesView={4} />
    </Container>
  </div>
    <TestimonialsSlider />
    <Footer />
  </div>
)
