import React from "react"
import HeaderLogado from "../components/header-logado"
import ContinueClassBanner from "../components/continueClassBanner/continueClassBanner"

import MessageTab from "../components/messageTab/messageTab"

import FilterMenu from "../components/filterMenu/filterMenu"

import YemSlider from "../components/yemSlider/yemSlider"

import ContainerFluid from "../components/containerFluid/containerFluid"
import ImagemBannerDeAula from "../images/classBanner.png"
import InstructorBanner from "../components/instructorBanner/instructorBanner"
import Footer from "../components/footer"

class areaAluno extends React.Component {
    render() {
        return(
            <div>
                <HeaderLogado/>
                <ContinueClassBanner
                completionLevel={"70%"}
                levelInfoText={"pratique mais e evolua"}
                levelName={"MULADHARA"}
                />
                <div className={"bgDarkGrey"}>
                    <ContainerFluid>
                        <div style={{padding: "1.66666rem 0"}}>
                            <MessageTab
                                iconClassName={"iconHappy"}
                                messageTitle={"Atenção"}
                                messageText={"Você ainda precisa confirmar o seu email, enviamos um link para vi******@gmail.****"}
                            />
                        </div>
                        <FilterMenu />
                    </ContainerFluid>

                    <YemSlider
                    titleClassName={"textSemiBold"}
                    titleText={"Continuar a praticar"}
                    />
                </div>

                <div className={"bgDarkGrey"}>
                    <YemSlider
                    titleClassName={"textBlack"}
                    titleText={"Novas aulas disponíveis"}
                    />
                </div>
                <div className={"bgDarkGrey"}>
                    <ContainerFluid>
                        <InstructorBanner
                            bannerTopTitle={"Seja instrutor de Yôga"}
                            bannerImg={ImagemBannerDeAula}
                            bannerTitle={"Técnica, Filosofia, Prática, Didática e Profissão"}
                            monthClass={"jan"}
                            yearClass={"2020"}
                            numberOfInstallments={"10"}
                            priceOfInstallments={"365"}
                        />
                    </ContainerFluid>
                </div>
                <Footer/>
            </div>
        )
    }
}
export default areaAluno;