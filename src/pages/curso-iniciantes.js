import React from "react"
import Header from "../components/header"
import IntroVideo from "../components/imageTextIntro/introWithVideo"
import ThreeBenefitsBlock from "../components/threeBenefitsBlock/threeBenefitsBlockDoubleCount"
import TwoCircleBlock from "../components/twoCircleBlock/twoCircleBlock"
import PracticeBlock from "../components/practiceBlock/practiceBlock" 
import SocialBlock from "../components/socialListColumn/socialListColumn"
import Container from "../components/container/container"
import StartTodayBlock from "../components/startTodayButtonBlock/startTodayButtonBlock"
import Testimonial from "../components/testimonials/testimonials"
import CommonQuestions from "../components/commonQuestions/commonQuestions"
import Footer from "../components/footer"

import EscolaStyles from "./page-styles/escola.module.scss"
import CursoIniciantesStyles from './page-styles/curso-iniciantes.module.scss'
import cursoUnicoStyles from "./page-styles/curso-unico.module.scss"

import EscolaBg from "../images/escolaBg.png"
import YogaBg from "../images/circleYogaBg.png"
import WomanWithHat from "../images/woman-with-hat.png"
import TwoPeopleYoga from '../images/two-yoga.png'


class cursoIniciantesPage extends React.Component {
    render() {
        return(
            <div>
                <Header/>

                <SocialBlock 
                    facebookUrl="https://pt-br.facebook.com/" 
                    youtubeUrl="https://www.youtube.com/?gl=BR&hl=pt"
                    instragramUrl="https://www.instagram.com/?hl=pt-br"
                />

                <IntroVideo
                    introImg={EscolaBg}
                    contentBlockCustomClass={EscolaStyles.introBlock}
                    titleCustomClass={EscolaStyles.escolaTitle, CursoIniciantesStyles.titleIntroVideo}
                    iconClass={"play"}
                    introTitle={"Yoga para iniciantes aulas grátis"}
                    firstIntroText={"a sua porta de entrada ao Yoga"}
                >
                    <ThreeBenefitsBlock 
                        linkScroll="#ondeVoceEstiver"
                        firstBlockIcon="usersIcon"
                        firstBlockPrefix="+"
                        countOfClasses={40000}
                        firstBlockSuffix=" alunos"
                        firstBlockText="começaram no Yoga com nossos cursos"
                        thirdBlockTitle="comece hoje mesmo"
                        thirdBlockButtonText="cadastrar"
                        thirdBlockText="nãe é preciso informar cartão de crédito"
                        secondBlockStyle="teste" 
                        className={EscolaStyles.threeBenefitsBlock, CursoIniciantesStyles.cursosIniciantes}
                    />
                </IntroVideo>

                <div id="ondeVoceEstiver">
                    <div className={["bgLightGrey", cursoUnicoStyles.userBlock].join(' ')}>
                        <Container internClass={["displayFlex flexWrap", cursoUnicoStyles.userBlockItem].join(' ')}>
                            <div className={["mdBlock"].join(' ')}>
                                <div className={["roundBorderFull", cursoUnicoStyles.userImg].join(' ')} style={{backgroundImage: ["url(", TwoPeopleYoga,")"].join(' ')}}></div>
                            </div>
                            <div className={["mdBlock displayFlex flexColumn flexFullCenter", CursoIniciantesStyles.blockUl].join(' ')}>
                                <h4 className={[cursoUnicoStyles.title, cursoUnicoStyles.userTitle].join(' ')}>Para quem é o curso?</h4>
                                <ul className={CursoIniciantesStyles.list}>
                                    <li>Pessoas que nunca praticaram Yoga antes</li>
                                    <li>Alunos iniciantes de Yoga que querem se tornar avançados</li>
                                    <li>Alunos avançados que querem rever os conceitos básicos da prática</li>
                                    <li>Pessoas que querem aumentar o rendimento no trabalho e estudos</li>
                                    <li>Pessoas que querem sair do sedentarismo</li>
                                    <li>Todos que buscam por autoconhecimento</li>
                                </ul>
                            </div>
                        </Container>
                    </div>
                </div>

                <TwoCircleBlock 
                    topBgImage={YogaBg}
                    topTitle={"Pratique do seu jeito"}
                    topText1={"Ao fazer o seu cadastro você terá acesso a duas"}
                    topText2={"sequências completas para iniciantes, e poderá escolher"}
                    topText3={"aquela que melhor se encaixa no seu perfil. Nas duas"}
                    topText4={"opções você aprenderá as técnicas fundamentais para"}
                    topText5={"seguir evoluindo no Yoga"}
                />

                <div className="lightGreyBg">
                    <Container>
                        <div className={["displayFlex flexFullCenter", EscolaStyles.practiceBlock].join(' ')}>
                            <div className="lgBlock displayFlex flexEnd">
                                <PracticeBlock 
                                    darkBg={false} 
                                    titleText={"Sempre com você"}
                                    practiceText={"Assista na sua TV, celular ou tablet, de forma simples e fácil"}
                                />
                            </div>
                            <div className="smBlock">
                                <StartTodayBlock></StartTodayBlock>
                            </div>
                        </div>
                    </Container>
                </div>

                <div className={["bgDarkGrey", EscolaStyles.testimonialBlock, CursoIniciantesStyles.testimonial ].join(' ')}>
                    <Testimonial></Testimonial>
                </div>

                <div className="bgDarkGrey normalPaddingTop">
                    <Container>
                        <CommonQuestions/>
                    </Container>
                </div>

                <Footer/>
            </div>
        )
    }
}

export default cursoIniciantesPage