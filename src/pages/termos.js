import React from "react"
import Header from "../components/header"
import SocialBlock from "../components/socialListColumn/socialListColumn"
import Container from "../components/container/container"
import ImageTextIntro from "../components/imageTextIntro/imageTextIntro"
import Testimonials from "../components/testimonials/testimonials"
import Footer from "../components/footer"

import privacyTermsStyles from "./page-styles/privacy-terms.module.scss"

export default () => (
    <div>
        <Header />
        <ImageTextIntro
            containerClass = {privacyTermsStyles.privacyTopContainer}
            titleText = {"Termos de privacidade"}
            infoTextClass = {"displayNone"}
            btnClass = {"displayNone"}
        >
            <SocialBlock 
                className={privacyTermsStyles.privacySocial}
                facebookUrl="https://pt-br.facebook.com/" 
                youtubeUrl="https://www.youtube.com/?gl=BR&hl=pt"
                instragramUrl="https://www.instagram.com/?hl=pt-br"
            />
        </ImageTextIntro>
            <div>
                <Container internClass={privacyTermsStyles.privacyContainer}>
                    <p className={privacyTermsStyles.privacyText}>Esta política descreve as formas como coletamos, armazenamos, usamos e protegemos suas informações pessoais. Você aceita essa política e concorda com tal coleta, armazenamento e uso quando se inscrever ou usar nossos produtos, serviços ou qualquer outro recurso, tecnologia ou funcionalidade que nós oferecemos ao acessar nosso site ou por qualquer outro meio (coletivamente “os Serviços do Yoga em Movimento”). Podemos alterar esta política a qualquer momento, divulgando uma versão revisada em nosso site. A versão revisada entrará em vigor assim que disponibilizada no site. Além disso, se a versão revisada incluir uma alteração substancial, avisaremos você com 30 dias de antecedência, divulgando o aviso sobre a alteração na página “Atualizações da política” do nosso site. Depois desse aviso de 30 dias, será considerado que você concorda com todas as emendas feitas a essa política.</p>
                    <p className={["textBlack", privacyTermsStyles.privacyText].join(' ')}>Importante: Política de Descadastramento (“Opt-out”)</p>
                    <p className={privacyTermsStyles.privacyText}>O usuário dos nossos serviços pode a qualquer momento deixar de receber comunicações do nosso site. Para tanto basta enviar um email para suporte@yogaemmovimento.com indicando o seu desejo de não mais receber comunicações, ou simplesmente clicar no link ‘remover’ ou de ‘unsubscribe’ contido no final de cada email.</p>
                    <p className={["textBlack", privacyTermsStyles.privacyText].join(' ')}>Como coletamos informações a seu respeito</p>
                    <p className={privacyTermsStyles.privacyText}>Quando você visita o site do Yoga em Movimento ou usa os Serviços do Yoga em Movimento, coletamos o seu endereço IP e as informações padrão de acesso à web como o tipo do seu navegador e as páginas que acessou em nosso site.</p>
                    <p>Informações de contato – o seu nome, endereço, telefone, e-mail, nome de usuário do Skype e outras informações semelhantes.</p>
                    <p>Informações financeiras – os números da conta bancária e do cartão de crédito que você forneceu quando adquirir os Serviços do Yoga em Movimento.</p>
                    <p className={privacyTermsStyles.privacyText}>Antes de permitir o uso dos Serviços do Yoga em Movimento, poderemos exigir que você forneça informações adicionais que poderemos usar para verificar sua identidade ou endereço ou gerenciar risco, como sua data de nascimento, número de registro nacional, nacionalidade e outras informações de identificação.</p>
                    <p className={privacyTermsStyles.privacyText}>Também podemos obter informações suas através de terceiros, como centros de crédito e serviços de verificação de identidade.</p>
                    <p className={privacyTermsStyles.privacyText}>Quando estiver usando os Serviços do Yoga em Movimento, coletaremos informações sobre suas transações e outras atividades suas em nosso site ou quando usar os Serviços do Yoga em Movimento e podemos coletar informações sobre seu computador ou outro dispositivo de acesso com finalidade de prevenção contra fraude.</p>
                    <p className={privacyTermsStyles.privacyText}>Por fim, podemos coletar informações adicionais de e sobre você por outros meios, como de contatos com nossa equipe de suporte ao cliente, de resultados de suas respostas a uma pesquisa, de interações com membros da família corporativa do Yoga em Movimento e de outras empresas.</p>
                    <p className={["textBlack", privacyTermsStyles.privacyText].join(' ')}>Como usamos cookies</p>
                    <p className={privacyTermsStyles.privacyText}>Quando você acessa nosso site, nós, incluindo as empresas que contratamos para acompanhar como nosso site é usado, podemos colocar pequenos arquivos de dados chamados “cookies” no seu computador.</p>
                    <p className={privacyTermsStyles.privacyText}>Enviamos um “cookie da sessão” para o seu computador quando você entra em sua conta ou usa os Serviços do Yoga em Movimento. Esse tipo de cookie nos ajuda a reconhecê-lo se visitar várias páginas em nosso site durante a mesma sessão, para que não precisemos solicitar a sua senha em todas as páginas. Depois que você sair ou fechar o seu navegador, esse cookie irá expirar e deixará de ter efeito.</p>
                    <p className={privacyTermsStyles.privacyText}>Também usamos cookies mais permanentes para outras finalidades, como para exibir o seu endereço de e-mail em nosso formulário de acesso, para que você não precise digitar novamente o endereço de e-mail sempre que entrar em sua conta.</p>
                    <p className={privacyTermsStyles.privacyText}>Codificamos nossos cookies para que apenas nós possamos interpretar as informações armazenadas neles. Você está livre para recusar nossos cookies caso o seu navegador permita, mas isso pode interferir no uso do nosso site.</p>
                    <p className={privacyTermsStyles.privacyText}>Nós e nossos prestadores de serviço também usamos cookies para personalizar nossos serviços, conteúdo e publicidade, avaliar a eficiência das promoções e promover confiança e segurança.</p>
                    <p className={privacyTermsStyles.privacyText}>Você pode encontrar cookies de terceiros ao usar os Serviços do Yoga em Movimento em determinados sites que não estão sob nosso controle (por exemplo, se você visualizar uma página da Web criada por terceiros ou usar um aplicativo desenvolvido por terceiros, um cookie poderá ser colocado por essa página ou aplicativo).</p>
                    <p className={["textBlack", privacyTermsStyles.privacyText].join(' ')}>Remarketing do Google</p>
                    <p className={privacyTermsStyles.privacyText}>Nós utilizamos o recurso de Remarketing do Google Adwords para veicular anúncios em sites de parceiros do Google. Este recurso permite identificar que você visitou o nosso site e assim o Google pode exibir o nosso anúncio para você em diferentes websites. Diversos fornecedores de terceiros, inclusive o Google, compram espaços de publicidade em sites da Internet. Nós eventualmente contratamos o Google para exibir nossos anúncios nesses espaços. Para identificar a sua visita no nosso site, tanto outros fornecedores de terceiros, quanto o Google, utilizam-se de cookies, de forma similar ao exposto na seção “Como usamos cookies”. Você pode desativar o uso de cookies pelo Google acessando o Gerenciador de preferências de anúncio.</p>
                    <p className={["textBlack", privacyTermsStyles.privacyText].join(' ')}>Como protegemos e armazenamos informações pessoais</p>
                    <p className={privacyTermsStyles.privacyText}>Ao longo desta política, usamos o termo “informações pessoais” para descrever informações que possam ser associadas a uma determinada pessoa e possam ser usadas para identificar essa pessoa. Nós não consideraremos como informações pessoais as informações que devem permanecer anônimas, para que elas não identifiquem um determinado usuário.</p>
                    <p className={privacyTermsStyles.privacyText}>Armazenamos e processamos suas informações pessoais em nossos computadores nos e as protegemos sob medidas de proteção físicas, eletrônicas e processuais. Usamos proteções de computador, como firewalls e criptografia de dados, aplicamos controles de acesso físico a nossos prédios e arquivos e autorizamos o acesso a informações pessoais apenas para os funcionários que precisem delas para cumprir suas responsabilidades profissionais.</p>
                    <p className={["textBlack", privacyTermsStyles.privacyText].join(' ')}>Como usamos as informações pessoais que coletamos</p>
                    <p className={privacyTermsStyles.privacyText}>Nossa finalidade principal ao coletar informações pessoais é fornecer a você uma experiência segura, tranquila, eficiente e personalizada. Para isso, usamos suas informações pessoais para:</p>
                    <p>fornecer os serviços e o suporte ao cliente solicitados;</p>
                    <p>processar transações e enviar avisos sobre as suas transaçõe</p>
                    <p>solucionar disputas, cobrar taxas e solucionar problemas;</p>
                    <p>impedir atividades potencialmente proibidas ou ilegais e garatir a aplicação do nosso Contrato do usuário;</p>
                    <p>personalizar, avaliar e melhorar nossos serviços, além do conteúdo e do layout do nosso site;</p>
                    <p>enviar materiais de marketing direcionados, avisos de atualização no serviço e ofertas promocionas com base</p>
                    <p className={privacyTermsStyles.privacyText}>nas suas preferências de comunicação; comparar informações, para uma maior precisão, e verificá-las com terceiros.</p>
                    <p className={["textBlack", privacyTermsStyles.privacyText].join(' ')}>Marketing</p>
                    <p className={privacyTermsStyles.privacyText}>Não vendemos nem alugamos suas informações pessoais para terceiros para fins de marketing sem seu consentimento explícito. Podemos combinar suas informações com as informações que coletamos de outras empresas e usá-las para melhorar e personalizar nossos serviços, conteúdo e publicidade. Se não desejar receber nossas mensagens de marketing nem participar de nossos programas de personalização de anúncios, basta indicar sua preferência mandando-nos um e-mail ou simplesmente clicar no link de descadastramento fornecido em todos os nossos emails.</p>
                    <p className={["textBlack", privacyTermsStyles.privacyText].join(' ')}>Como compartilhamos informações pessoais com outras partes</p>
                    <p className={privacyTermsStyles.privacyText}>Podemos compartilhar suas informações pessoais com:</p>
                    <p className={privacyTermsStyles.privacyText}>Membros da família corporativa do Yoga em Movimento para fornecer conteúdo, produtos e serviços conjunto (como registro, transações e suporte ao cliente) para ajudar a detectar e impedir atos potencialmente ilegais e violações de nossas políticas, além de cooperar nas decisões quanto a seus produtos, serviços e comunicações. Os membros da nossa família corporativa usarão essas informações para lhe enviar comunicações de marketing apenas se você tiver solicitado seus serviços.</p>
                    <p className={privacyTermsStyles.privacyText}>Fornecedores do serviços sob contrato que colaboram em partes de nossas operações comerciais; (prevenção contra fraude, atividades de cobrança, marketing, serviços de tecnologia). Nossos contratos determinam que esses fornecedores de serviço só usem suas informações em relação aos serviços que realizam para nós, e não em benefício próprio.</p>
                    <p className={privacyTermsStyles.privacyText}>Empresas com as quais pretendemos nos fundir ou adquirir. (Se ocorrer uma fusão, exigiremos que a nova entidade constituída siga essa política de privacidade com relação às suas informações pessoais. Se suas informações pessoais puderem ser usadas contra essa política, você receberá um aviso prévio).</p>
                    <p className={privacyTermsStyles.privacyText}>Autoridades policiais, oficiais do governo ou outros terceiros quando:</p>
                    <p className={privacyTermsStyles.privacyText}>formos obrigados a isso por intimação, decisão judicial ou procedimento legal semelhante,
precisamos fazer isso para estar em conformidade com a lei ou com as regras de associação de cartão de crédito</p>
                    <p className={privacyTermsStyles.privacyText}>estivermos cooperando com uma investigação policial em andamento, acreditamos, de boa fé, que a divulgação das informações pessoais é necessária para impedir danos físicos ou perdas financeiras, para reportar atividade ilegal suspeita ou investigar violações do nosso Contrato do usuário.</p>
                    <p>Outros terceiros com seu consentimento ou orientação para tanto. Note que esses terceiros podem estar em outros países nos quais a legislação sobre o processamento de informações pessoais seja menos rígida do que a do seu país.</p>
                </Container>
            </div>
        <div className="bgDarkGrey">
            <Testimonials />
        </div>
        <Footer />
    </div>
)