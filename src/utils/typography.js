import Typography from "typography"


const typography = new Typography({
    googleFonts: [
        {
          name: 'Muli',
          styles: [
            '300',
            '400',
            '700',
            '900'
          ],
        },
      ],
      baseFontSize: '18px',
    scaleRatio: 2.777,
    baseLineHeight: 1.333,
    bodyGray: 12,
    bodyWeight: 300,
    headerWeight: 900,
    headerGray: 12,
    blockMarginBottom: 0,
    headerFontFamily: ['Muli','sans-serif'],
    bodyFontFamily: ['Muli', 'sans-serif'],
})

export const { scale, rhythm, options } = typography
export default typography